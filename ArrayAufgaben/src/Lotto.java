
public class Lotto {

	public static void main(String[] args) {
		//Initilisieren des Arrays und Speichern der Lottozahlen in Array
		int[] arrayLotto = { 3 , 7 , 12 , 18, 37 , 42 };
		boolean enthalten12 = false, enthalten13 = false;
		
		//Ausgabe der Lottozahlen / Aufgabe 4a und Pr�fen ob 12 und 13 vorkommen / Aufgabe 4b
		System.out.print("[ ");
		for(int i = 0; i < arrayLotto.length; i++) 
		{
			System.out.print(arrayLotto[i] + " ");
			
			if(arrayLotto[i] == 12)
				enthalten12 = true;
			
			if(arrayLotto[i] == 13)
				enthalten13 = true;
		}
		System.out.println("]");
		
		//Ausgabe ob 12 oder 13 enthalten sind / Aufgabe 4b
		if(enthalten12 == true) 
		{
			System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
		}
		else 
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
		
		if(enthalten13 == true) 
		{
			System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
		}
		else 
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
	}

}
