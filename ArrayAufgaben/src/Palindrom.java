import java.util.Scanner;
public class Palindrom {

	public static void main(String[] args) {
		
		//Initialisierung Array f�r die 5 Char
		Scanner scan = new Scanner(System.in);
		char[] arrayPali = new char[5];
		
		//Aufforderung zu Eingabe der 5 Char
		System.out.println("Geben Sie 5 mal ein Zeichen ein.");

		//Z�hlschleife zum Speichern der Eingaben in Array
		for(int i = 0; i < arrayPali.length; i++) 
		{
			arrayPali[i]= scan.next().charAt(0);
		}
		
		//Ausgabe der Arraywerte von hinten nach vorne
		for(int i = arrayPali.length - 1; i >= 0; i--) 
		{
			System.out.print(arrayPali[i] + " ");
		}
	}

}
