
public class Zahlen {

	public static void main(String[] args) {

		//ganzahliges Array mir der L�nge 10
		int [] arrayZahlen = new int[10];
		
		//Z�hlschleife zum F�llen des Arrays mit Werten zwischen 0 und 9 und Ausgeben der Werte des gesamten Arrays
		for(int i = 0; i < arrayZahlen.length; i++) 
		{
			arrayZahlen[i] = i;
			System.out.print(arrayZahlen[i] + " ");
		}
	}

}
