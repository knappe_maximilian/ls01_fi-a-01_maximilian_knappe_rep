
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
		System.out.printf("Hallo \t du\n");
		System.out.print("Hallo du");

		System.out.printf("|%20s|%n","123456789");
		System.out.printf("|%-20s|%n","123456789");
		System.out.printf("|%-20.3s|%n","123456789");
		
		System.out.printf("|%-20d|%n",123456789);
		
		System.out.printf("|%-20f|%n",12345.6789);
		System.out.printf("|%+-20f|%n",12345.6789);
		
		System.out.printf("|%-20.2f|%n",12345.6789);
		
		System.out.printf("Der Preis von %s ist %.2f Euro.", "Monitor", 109.50);

		System.out.println("    ** ");
		System.out.println("*\t *");
		System.out.println("*\t *");
		System.out.println("    ** ");
				
		int alter = 30;
		String name = "max";
		System.out.print("Ich bin " + name + " und bin " + alter + " jung");
	

		
	}

}
