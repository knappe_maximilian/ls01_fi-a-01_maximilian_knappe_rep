
public class Ausgabenformatierungsaufgabe1 {

	public static void main(String[] args) {
/*
//		Variante 1		//
		System.out.print("   **   \n");
		System.out.print("*      *\n");
		System.out.print("*      *\n");
		System.out.print("   **   ");
*/		
// 		Variante 2		//
		System.out.printf("%4s", "*");
		System.out.print("* \n");
		System.out.printf("%-4s", "*");
		System.out.printf("%4s\n", "*");
		System.out.printf("%-4s", "*");
		System.out.printf("%4s\n", "*");
		System.out.printf("%4s", "*");
		System.out.print("* \n");
	

	}

}
