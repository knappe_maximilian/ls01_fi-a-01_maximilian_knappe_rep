
public class Ausgabenformatierungsaufgabe2 {

	public static void main(String[] args) {
//		FALSCH da RechtsbŁndig		//
		
/*
		int n = 1;
		String gleich = "=";
		String Ausrufezeichen = "!";
		String Mal = "*";
		
		System.out.printf("%d", 0);
		System.out.printf("%-4s","" + Ausrufezeichen + "");
		System.out.printf("%-19s", "" + gleich + "");
		System.out.printf("" + gleich + "");
		System.out.printf("%4d\n", n);  
		
		System.out.printf("%d", n);
		System.out.printf("%-4s","" + Ausrufezeichen + "");
		System.out.printf("%s", "" + gleich + "");
		System.out.printf("%2d",n);
		System.out.printf("%17s", "" + gleich + "");
		System.out.printf("%4d\n", n); 

		System.out.printf("%d", n);
		System.out.printf("%-4s","" + Ausrufezeichen + "");
		System.out.printf("%s", "" + gleich + "");
		System.out.printf("%2d",n);
		System.out.printf("%2s", "" + Mal + "");
		System.out.printf("%2d",n*2);
		System.out.printf("%13s", "" + gleich + "");
		System.out.printf("%4d\n", n*2); 
		
		System.out.printf("%d", n*2);
		System.out.printf("%-4s","" + Ausrufezeichen + "");
		System.out.printf("%s", "" + gleich + "");
		System.out.printf("%2d",n);
		System.out.printf("%2s", "" + Mal + "");
		System.out.printf("%2d",n*2);
		System.out.printf("%2s", "" + Mal + "");
		System.out.printf("%2d",n*3);
		System.out.printf("%9s", "" + gleich + "");
		System.out.printf("%4d\n", n*6); 

		System.out.printf("%d", n*3);
		System.out.printf("%-4s","" + Ausrufezeichen + "");
		System.out.printf("%s", "" + gleich + "");
		System.out.printf("%2d",n);
		System.out.printf("%2s", "" + Mal + "");
		System.out.printf("%2d",n*2);
		System.out.printf("%2s", "" + Mal + "");
		System.out.printf("%2d",n*3);
		System.out.printf("%2s", "" + Mal + "");
		System.out.printf("%2d",n*4);
		System.out.printf("%5s", "" + gleich + "");
		System.out.printf("%4d\n", n*24); 
		
		System.out.printf("%d", n*4);
		System.out.printf("%-4s","" + Ausrufezeichen + "");
		System.out.printf("%s", "" + gleich + "");
		System.out.printf("%2d",n);
		System.out.printf("%2s", "" + Mal + "");
		System.out.printf("%2d",n*2);
		System.out.printf("%2s", "" + Mal + "");
		System.out.printf("%2d",n*3);
		System.out.printf("%2s", "" + Mal + "");
		System.out.printf("%2d",n*4);
		System.out.printf("%2s", "" + Mal + "");
		System.out.printf("%2d",n*5);
		System.out.printf("%s", "" + gleich + "");
		System.out.printf("%4d\n", n*120); 



//		Korrekt da LinksbŁndig	
		int n = 1;
		String gleich = "=";
		String Ausrufezeichen = "!";
		String Mal = "*";
		
		System.out.printf("%d", 0);
		System.out.printf("%-5s","" + Ausrufezeichen + "");
		System.out.printf("%-20s", "" + gleich + "");
		System.out.printf("" + gleich + "");
		System.out.printf("%4d\n", n);  
		
		System.out.printf("%d", n);
		System.out.printf("%-5s","" + Ausrufezeichen + "");
		System.out.printf("%-2s", "" + gleich + "");
		System.out.printf("%d",n);
		System.out.printf("%18s", "" + gleich + "");
		System.out.printf("%4d\n", n); 

		System.out.printf("%d", n);
		System.out.printf("%-5s","" + Ausrufezeichen + "");
		System.out.printf("%-2s", "" + gleich + "");
		System.out.printf("%-2d",n);
		System.out.printf("%-2s", "" + Mal + "");
		System.out.printf("%-2d",n*2);
		System.out.printf("%13s", "" + gleich + "");
		System.out.printf("%4d\n", n*2); 
		
		System.out.printf("%d", n*2);
		System.out.printf("%-5s","" + Ausrufezeichen + "");
		System.out.printf("%-2s", "" + gleich + "");
		System.out.printf("%-2d",n);
		System.out.printf("%-2s", "" + Mal + "");
		System.out.printf("%-2d",n*2);
		System.out.printf("%-2s", "" + Mal + "");
		System.out.printf("%-2d",n*3);
		System.out.printf("%9s", "" + gleich + "");
		System.out.printf("%4d\n", n*6); 

		System.out.printf("%d", n*3);
		System.out.printf("%-5s","" + Ausrufezeichen + "");
		System.out.printf("%-2s", "" + gleich + "");
		System.out.printf("%-2d",n);
		System.out.printf("%-2s", "" + Mal + "");
		System.out.printf("%-2d",n*2);
		System.out.printf("%-2s", "" + Mal + "");
		System.out.printf("%-2d",n*3);
		System.out.printf("%-2s", "" + Mal + "");
		System.out.printf("%-2d",n*4);
		System.out.printf("%5s", "" + gleich + "");
		System.out.printf("%4d\n", n*24); 
		
		System.out.printf("%d", n*4);
		System.out.printf("%-5s","" + Ausrufezeichen + "");
		System.out.printf("%-2s", "" + gleich + "");
		System.out.printf("%-2d",n);
		System.out.printf("%-2s", "" + Mal + "");
		System.out.printf("%-2d",n*2);
		System.out.printf("%-2s", "" + Mal + "");
		System.out.printf("%-2d",n*3);
		System.out.printf("%-2s", "" + Mal + "");
		System.out.printf("%-2d",n*4);
		System.out.printf("%-2s", "" + Mal + "");
		System.out.printf("%-2d",n*5);
		System.out.printf("%s", "" + gleich + "");
		System.out.printf("%4d\n\n", n*120); 

*/
		
// 		Variante 2		//
		
		byte n = 1;
		System.out.printf("%d", n*0);
		System.out.printf("%-4s", "!");
		System.out.printf("%-20s", "=");
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", n);
		
		System.out.printf("%d", n); 	
		System.out.printf("%-4s", "!");
		System.out.printf("%-2s", "=");
		System.out.printf("%-18d", n);
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", n);
		
		System.out.printf("%d", n*2);
		System.out.printf("%-4s", "!");
		System.out.printf("%-2s", "=");
		System.out.printf("%-2d", n);
		System.out.printf("%-2s", "*");
		System.out.printf("%-14d", n*2);
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", n*2);
		
		System.out.printf("%d", n*3);
		System.out.printf("%-4s", "!");
		System.out.printf("%-2s", "=");
		System.out.printf("%-2d", n);
		System.out.printf("%-2s", "*");
		System.out.printf("%-2d", n*2);
		System.out.printf("%-2s", "*");
		System.out.printf("%-10d", n*3);
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", n*6);
		
		System.out.printf("%d", n*4);
		System.out.printf("%-4s", "!");
		System.out.printf("%-2s", "=");
		System.out.printf("%-2d", n);
		System.out.printf("%-2s", "*");
		System.out.printf("%-2d", n*2);
		System.out.printf("%-2s", "*");
		System.out.printf("%-2d", n*3);
		System.out.printf("%-2s", "*");
		System.out.printf("%-6d", n*4);
		System.out.printf("%s", "=");
		System.out.printf("%4d\n", n*24);
		
		System.out.printf("%d", n*5);
		System.out.printf("%-4s", "!");
		System.out.printf("%-2s", "=");
		System.out.printf("%-2d", n);
		System.out.printf("%-2s", "*");
		System.out.printf("%-2d", n*2);
		System.out.printf("%-2s", "*");
		System.out.printf("%-2d", n*3);
		System.out.printf("%-2s", "*");
		System.out.printf("%-2d", n*4);
		System.out.printf("%-2s", "*");
		System.out.printf("%-2d", n*5);
		System.out.printf("%s", "=");
		System.out.printf("%4d", n*120);
		

	}

}
