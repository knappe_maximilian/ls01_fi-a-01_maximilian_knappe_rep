﻿import java.util.Scanner;		//Importieren der Scanner-Fähigkeit

class Fahrkartenautomat
{
		public static void main(String[] args) 								//Main Methode und Aufruf der verschiedenen Ablaufmethoden für EVA-Prozess
	{			
		int lag = 250;
	       LogoBVG();														//Logo aufrufe
	       warte(lag);
	       double zuZahlenderBetrag = fahrkartenbestellungErfassen(lag);	//Erfassen der Bestellung und Übergabe des Gesamtwertes an Variable
	       if(zuZahlenderBetrag > 0)
	       {
	       double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);	//Übergabe Gesamtbetrag an Methode und Bezahlen der Tickets durch Münzeinwurf und Übergabe des Rückgabewertes an Variable
	       fahrkartenAusgeben(lag);											//Methode Fahrkartenausgabe mit Druckprozessimulation
	       rueckgeldAusgeben(rückgabebetrag);								//Übergabe des Rückgabewertes an Rückgeldmethode und Ausführen der Methode

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+	//Infotext mit Erinnerung zu Stempeln des Ticktes
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");		//Verabschiedung
	       //Ende des Programmes
	       warte(lag);
	       }
	       //Neustart Programm
	       System.out.println("\n\nFahrkartenautomat wird vorbereitet");
	       for(int i = 0; i < 8; i++) 									// Variable Datentyp Integer
	       {
	          System.out.print("=");
	          warte(lag);												//Wartezeit 250ms
	       }
	       System.out.println("\n\n");
	       main(args);													//Der Fahrkartenautomat soll nach Abwicklung eines Kaufvorgangs sofort für weitere Kaufvorgänge bereitstehen.
    }
	public static double fahrkartenbestellungErfassen(int lag) 			//Methode (ohne Übergabeparameter) zur Erfassung der Bestellung
	{ 			
		Scanner tastatur = new Scanner(System.in);						// Initialisierung Scanner 'tastatur'
		double preisTicket; 											// Variable für Ticketpreise
		int auswahlFahrkarte;  											// Variable für Fahrkartenart
		int anzahlTickets;  											// Variable für Ticketanzahl
		double preisGesamt = 0; 										// Variable für Gesamtpreis, ergibt sich aus Anzahl der Tickets und dem Ticketpreis
		double preisGesamtBestellung = 0;								// Variable für Gesamtpreis des Bestellvorgangs, ergibt aus der Summe der Ticketpreise
		int nummerBestellung = 0;										// Laufnummer für das Array 'arrayBestellung'
		String[] arrayBestellungBezeichnung = new String[100];			// Array zum Speichern der Ticketarten im Warenkorb (100 Bestelldurchgänge möglich)
		double[] arrayBestellungPreis = new double[100];				// Array zum Speichern der Ticketpreise im Warenkorb
		int[] arrayBestellungAnzahl = new int[100];						// Array zum Speichern der Ticketanzahl im Warenkorb
		
			trenner();
			System.out.println("Fahrkartenbestellvorgang");	 			//Maske Fahrkartenauswahl 'Fahrkartenbestellvorgang'
			trenner();
			System.out.println("Wählen Sie Ihre Wunschfahrkarte aus.           Nummer eingeben\u2193"); //Aufforderung zur Auswahl der Fahrkartenart
			trenner();
			warte(lag);
			System.out.printf("%-53s%s\n%-53s%s\n%-53s%s\n%-53s%s\n%-53s%s\n%-53s%s\n","Kurzstrecke","[1]", "Einzelfahrschein","[2]","Tageskarte","[3]","Kleingruppen-Tageskarte","[4]","Anschlussfahrausweis","[5]","Gesamtes Angebot","[6]");
			trenner();
			int eingabe = 0;
			boolean richtigeeingabe = false;								//Wahrheitswert für richtige Eingabe ( 1 -6)
			while(richtigeeingabe == false)									//Kopfgesteuerte Schleife, solange keine richtige Eingabe getätigt wurde
			{
				System.out.print("Ihre Wahl: "); 							//Aufforderung zu Eingabe der Tickerart
				eingabe = tastatur.nextInt();							//Eingabe Auswahl
				trenner();
				if((eingabe > 0) && (eingabe < 7))							//Kontrollstruktur, ob korrekte Eingabe getätigt wurde
				{
					System.out.printf("%s%19s%10s%25s%12s\n","\u2193Art","\u2193Tarif","\u2193Zone","\u2193Preis","\u2193Eingabe"); //Tabellenkopf
					trenner();
				}
				richtigeeingabe = angebotanzeige(eingabe, lag);				//Wahrheitswert Übergabe aus Methode, wenn richtig, dann wird Schleife verlassen
			}
			
		boolean bestellungfertig = false;						// Variable zum Speichern des Bestellzustandes (false = Bestellung im Gange, true = Bestellung fertig und Bezahlvorgang starten)
		while (bestellungfertig == false)						//Solanage Bestellung nicht abgeschlossen wurde
		{
			//Wahl der Fahrkartenart durch Benutzer
			System.out.println("Bezahlen oder bei leerem Warenkorb erfolgt Neustart -->       (0)"); 			//Hinweis auf Ende Bestellung
			System.out.print("Ihre Wahl: "); 					//Aufforderung zu Eingabe der Tickerart
			auswahlFahrkarte = tastatur.nextInt();				//Eingabe Tickerart
		
			//Check ob Bestellung ist fertig (0 = nein, 1 = ja --> Bezahlung Fahrkarten im Warenkorb)
			if (auswahlFahrkarte == 0)
			{
				bestellungfertig = true; 						//direkt zum Ende Erfassung der Bestellung
			}
			else												//Bestellung fortsetzen
			{	//Übertragung der ArrayGrenzen je nach Eingabe für den Check auf ArrayGrenzen
				int arrayStart = 0;						
				int arrayEnde = 0;
				if(eingabe == 1)		//KS
				{
					arrayStart = 0;
					arrayEnde = 5;
				}
				else if(eingabe == 2)	//ES
				{
					arrayStart = 4;
					arrayEnde = 17;
				}
				else if(eingabe == 3)	//TK
				{
					arrayStart = 16;
					arrayEnde = 23;
				}
				else if(eingabe == 4)	//KG-TK
				{
					arrayStart = 2;
					arrayEnde = 26;
				}
				else if(eingabe == 5)	//AF
				{
					arrayStart = 25;
					arrayEnde = 28;
				}
				else if(eingabe == 6)	//Alle
				{
					arrayStart = 0;
					arrayEnde = 28;
				}
				
				//Check Eingabe auf Arraygrenzen
				while ((auswahlFahrkarte <= arrayStart) || (auswahlFahrkarte >= arrayEnde))	//Wenn Eingabe der Ticketart ist außerhalb des Arrays der Methoden fahrkartenArten und fahrkartenPreis
				{
					System.out.println(" >>falsche Eingabe<<");		//Hinweis auf falsche Eingabe für Tickerart
					trenner();
					System.out.println("Bezahlen --> (0)"); 		//Hinweis auf Ende Bestellung
					System.out.print("Ihre Wahl: "); 				//Aufforderung zu Eingabe der Tickerart
					auswahlFahrkarte = tastatur.nextInt();			//korrigierte Eingabe Tickerart
				}
				auswahlFahrkarte -= 1;								//Anpassung Eingabe und Arrayanzahl (beginnt bei 0)
			
				//Anzeige der Auswahl der Fahrkartenart und Fahrkartenpreis
				System.out.print(fahrkartenArten(auswahlFahrkarte));								//Ausgabe Fahrkartenauswahl mittels Aufruf der Methode mit Übergabe der Nummer für das Array
				System.out.printf("%s%.2f%5s\n" ," [", fahrkartenPreis(auswahlFahrkarte), "EUR]");	//Ausgabe Fahrkartenpreis mittels Aufruf der Methode mit Übergabe der Nummer für das Array
				System.out.print("Ticketanzahl: ");													//Aufforderung zur Eingabe der Ticketanzahl
			
				//Ticketanzahl
				anzahlTickets = tastatur.nextInt();									//Eingabe Ticketanzahl
				anzahlTickets = fahrkartenanzahlKontrolle(anzahlTickets);			//Kontrolle Ticketanzahl ist zwischen 1 und 10.
				preisTicket = fahrkartenPreis(auswahlFahrkarte);					//Fahrkartenpreis aus dem Array der fahrkartenPreis-Methode aufrufen und Speichern in preisTicket-Variable
			
				//Anzeige der Auswahl der Fahrkartenanzahl, Fahrkartenart sowie summierten Fahrkartenpreis 
				System.out.print(anzahlTickets + "x " + fahrkartenArten(auswahlFahrkarte)); 							//Ausgabe Fahrkartenanzahl durch Aufruf Variable und Ausgabe Fahrkartenart durch Aufruf Methode mit Übergabe der Variable 'auswahlFahrkarte'
				System.out.printf("%s%.2f%5s\n" ," [", (fahrkartenPreis(auswahlFahrkarte)  * anzahlTickets), "EUR]");	//Ausgabe Fahrkartenpreis durch Aufruf Methode mit Übergabe der Variable 'auswahlFahrkarte'
			
				//Speichern der Fahrkartenanzahl, Fahrkartenart und summierten Fahrkartenpreis in den entsprechenden Arrays zur Ausgabe im Warenkorb
				arrayBestellungBezeichnung[nummerBestellung] =  fahrkartenArten(auswahlFahrkarte); 						//Speichern der Fahrkart des Bestellvorganges im Array durch Aufruf Methode mit Übergabe
				arrayBestellungPreis[nummerBestellung] =  (fahrkartenPreis(auswahlFahrkarte) * anzahlTickets);	//Speichern des Fahrkpreises des Bestellvorganges im Array durch Aufruf Methode mit Übergabe
				arrayBestellungAnzahl[nummerBestellung] = anzahlTickets;										//Speichern der Fahrkanzahl des Bestellvorganges im Array durch Aufruf Methode mit Übergabe
				preisGesamt = preisTicket * anzahlTickets; 														//Berechnung des Gesamtpreises im Bestelldurchgang
				preisGesamtBestellung = preisGesamtBestellung + preisGesamt;  									//Berechnung des Gesamtpreises des Warenkorbs
				trenner();
			
				//Warenkorb
				System.out.println("Warenkorb:");																//Ausgabe des gesamten Warenkorbs druch Aufruf der drei Arrays, 'i' als Zähler
				for (int i = 0; i <= nummerBestellung; i++)													//Zählschleife mit Variable 'i', wird je Durchgang um eins erhöht
				{
					System.out.print(arrayBestellungAnzahl[i] + "x " + arrayBestellungBezeichnung[i]);					//Ausgabe des Inhaltes in des Arrays nach Wert des Zählers 'i'
					System.out.printf("%s%.2f%5s\n" ," [", arrayBestellungPreis[i], "EUR]");					//Ausgabe des Inhaltes im Array nach Wert des Zählers 'i'
				}
				
				System.out.printf("%s%4.2f%4s\n", "Summe: " , preisGesamtBestellung , "EUR");				//Ausgabe der Gesamtsumme des Warenkorbs
			
				nummerBestellung += 1;																		//Erhöhung des Bestelldurchgangs, Vorbereitung für den nächsten Bestellvorgangs		
			trenner();
			}
										
		}
		return preisGesamtBestellung;											//Übergabe des Gesamtpreises zur Verwendung außerhalb der Methode
	}
	
	public static boolean angebotanzeige(int eingabe, int lag)			//Methode zum Anzeigen des Angebotes, je nach Auswahl und Übergabe des Wahrheitswertes "richtigeEingabe"
	{
		boolean richtigeEingabe = false;
		if(eingabe == 6)
		{
			richtigeEingabe = true;
			//Aufruf der Ticketarten und den zugehörigen Ticketpreisen in Zählschleife (gesamtes Angebote)
			for (int i = 0; i <=26; i++) 									
				{
				System.out.println(fahrkartenArten(i) + " --> [" + fahrkartenPreis(i) + " EUR] (" + (i + 1) +")"); //Ausgabe der Ticketarten und den zugehörigen Ticketpreisen
				warte(lag);
				}
			trenner();
		}
		else if(eingabe == 1)
		{
			richtigeEingabe = true;
			//Aufruf der Ticketarten und den zugehörigen Ticketpreisen in Zählschleife (Kurzstrecke)
			for (int i = 0; i <=3; i++) 									
			{
				System.out.println(fahrkartenArten(i) + " --> [" + fahrkartenPreis(i) + " EUR] (" + (i + 1) +")"); //Ausgabe der Ticketarten und den zugehörigen Ticketpreisen
				warte(lag);
			}
			trenner();
		}
		else if(eingabe == 2)
		{
			richtigeEingabe = true;
			//Aufruf der Ticketarten und den zugehörigen Ticketpreisen in Zählschleife (Einzelfahrschein)
			for (int i = 4; i <=15; i++) 									
			{
				System.out.println(fahrkartenArten(i) + " --> [" + fahrkartenPreis(i) + " EUR] (" + (i + 1) +")"); //Ausgabe der Ticketarten und den zugehörigen Ticketpreisen
				warte(lag);
			}
			trenner();
		}
		else if(eingabe == 3)
		{
			richtigeEingabe = true;
			//Aufruf der Ticketarten und den zugehörigen Ticketpreisen in Zählschleife (Tageskarte)
			for (int i = 16; i <=21; i++) 									
			{
				System.out.println(fahrkartenArten(i) + " --> [" + fahrkartenPreis(i) + " EUR] (" + (i + 1) +")"); //Ausgabe der Ticketarten und den zugehörigen Ticketpreisen
				warte(lag);
			}
			trenner();
		}
		else if(eingabe == 4)
		{
			richtigeEingabe = true;
			//Aufruf der Ticketarten und den zugehörigen Ticketpreisen in Zählschleife (Kleingruppen-Tageskarte)
			for (int i = 22; i <=24; i++) 									
			{
				System.out.println(fahrkartenArten(i) + " --> [" + fahrkartenPreis(i) + " EUR] (" + (i + 1) +")"); //Ausgabe der Ticketarten und den zugehörigen Ticketpreisen
				warte(lag);
			}
			trenner();
		}
		else if(eingabe == 5)
		{
			//Aufruf der Ticketarten und den zugehörigen Ticketpreisen in Zählschleife (Anschlussfahrausweis)
			richtigeEingabe = true;
			for (int i = 25; i <=26; i++) 									
			{
				System.out.println(fahrkartenArten(i) + " --> [" + fahrkartenPreis(i) + " EUR] (" + (i + 1) +")"); //Ausgabe der Ticketarten und den zugehörigen Ticketpreisen
				warte(lag);
			}
			trenner();
		}
		else
		{
			System.out.println("Falsche Eingabe");
			trenner();
		}
		
		return richtigeEingabe;
	}
	
	public static int fahrkartenanzahlKontrolle(int anzT) 			//Methode Kontrolle der Ticketanzahl auf ungültigen Wert
	{			
		Scanner tastatur = new Scanner(System.in);
		while ((anzT > 10) || (anzT < 1)){							//Kopfgesteuerte Schleife mit Bedingungen Ticketanzahl unter 0 und über 10												//Wenn nicht zwischen 1 und 10, dann automatisch 1 
			 trenner();
			 System.out.println("Sie haben einen ungültigen Wert als Fahrkartenanzahl eingegeben!"); 	//Hinweis auf falsche Eingabe der Fahrkartenanzahl
			 System.out.println("Bitte eine Fahrkartenanzahl zwischen 1 und 10 eingeben.");			//Bitte um korregierte Eingabe der Fahrkartenanzahl
			 trenner();
			 System.out.print("Ticketanzahl: ");					//Aufforderung zur korrigierten Eingabe der Ticketanzahl
			 anzT = tastatur.nextInt();								//erneute Eingabe der Fahrkartenanzahl
		 }
		return anzT;												//Übergabe des korrigierten Fahrkartenanzahlwertes für die Verwendung außerhalb der Methode
	}
	
	public static String fahrkartenArten(int art) 					//Methode mit Array zum Speichern der Fahrkartenarten
	{
		String[] arrayFahrkartenarten = new String[27];				//Initialisieren des Arrays der Fahrkartenarten mit 28 Einträgen
		arrayFahrkartenarten[0] = "Kurzstrecke      Regeltarif                    ";
		arrayFahrkartenarten[1] = "Kurzstrecke      ermäßigt                      ";
		arrayFahrkartenarten[2] = "Kurzstrecke      Regeltarif     4-Fahrten-Karte";
		arrayFahrkartenarten[3] = "Kurzstrecke      ermäßigt       4-Fahrten-Karte";
		arrayFahrkartenarten[4] = "Einzelfahrschein Regeltarif AB                 ";
		arrayFahrkartenarten[5] = "Einzelfahrschein Regeltarif BC                 ";
		arrayFahrkartenarten[6] = "Einzelfahrschein Regeltarif ABC                ";
		arrayFahrkartenarten[7] = "Einzelfahrschein ermäßigt   AB                 ";
		arrayFahrkartenarten[8] = "Einzelfahrschein ermäßigt   BC                 ";
		arrayFahrkartenarten[9] = "Einzelfahrschein ermäßigt   ABC                ";
		arrayFahrkartenarten[10] = "Einzelfahrschein Regeltarif AB  4-Fahrten-Karte";
		arrayFahrkartenarten[11] = "Einzelfahrschein Regeltarif BC  4-Fahrten-Karte";
		arrayFahrkartenarten[12] = "Einzelfahrschein Regeltarif ABC 4-Fahrten-Karte";
		arrayFahrkartenarten[13] = "Einzelfahrschein ermäßigt   AB  4-Fahrten-Karte";
		arrayFahrkartenarten[14] = "Einzelfahrschein ermäßigt   BC  4-Fahrten-Karte";
		arrayFahrkartenarten[15] = "Einzelfahrschein ermäßigt   ABC 4-Fahrten-Karte";
		arrayFahrkartenarten[16] = "Tageskarte       Regeltarif AB                 ";
		arrayFahrkartenarten[17] = "Tageskarte       Regeltarif BC                 ";
		arrayFahrkartenarten[18] = "Tageskarte       Regeltarif ABC                ";
		arrayFahrkartenarten[19] = "Tageskarte       ermäßigt   AB                 ";
		arrayFahrkartenarten[20] = "Tageskarte       ermäßigt   BC                 ";
		arrayFahrkartenarten[21] = "Tageskarte       ermäßigt   ABC                ";
		arrayFahrkartenarten[22] = "Kleingruppen-Tageskarte     AB                 ";
		arrayFahrkartenarten[23] = "Kleingruppen-Tageskarte     BC                 ";
		arrayFahrkartenarten[24] = "Kleingruppen-Tageskarte     ABC                ";
		arrayFahrkartenarten[25] = "Anschlussfahrausweis  Regeltarif               ";
		arrayFahrkartenarten[26] = "Anschlussfahrausweis  ermäßigt                 ";
		return arrayFahrkartenarten[art];							//Übergabe der Fahrkartenart als String zur Ausgabe außerhalb der Methode
	}
	
	public static double fahrkartenPreis(int preis) 				//Methode mit Array zum Speichern der Fahrkartenpreise
	{
		double[] arrayFahrkartenpreise = new double[27];			//Initialisieren des Arrays der Fahrkartenpreise mit 28 Einträgen
		arrayFahrkartenpreise[0] = 1.90;	//KS R
		arrayFahrkartenpreise[1] = 1.40;	//KS e 
		arrayFahrkartenpreise[2] = 5.60;	//KS R     4FK
		arrayFahrkartenpreise[3] = 4.40;	//KS e     4FK
		arrayFahrkartenpreise[4] = 2.90;	//ES R AB
		arrayFahrkartenpreise[5] = 3.30;	//ES R BC
		arrayFahrkartenpreise[6] = 3.60;	//ES R ABC
		arrayFahrkartenpreise[7] = 1.80;	//ES e AB
		arrayFahrkartenpreise[8] = 2.30;	//ES e BC
		arrayFahrkartenpreise[9] = 2.60;	//ES e ABC
		arrayFahrkartenpreise[10] = 9.00;	//ES R AB  4FK
		arrayFahrkartenpreise[11] = 12.00;	//ES R BC  4FK
		arrayFahrkartenpreise[12] = 13.20;	//ES R ABC 4FK
		arrayFahrkartenpreise[13] = 5.60;	//ES e AB  4FK
		arrayFahrkartenpreise[14] = 8.40;	//ES e BC  4FK
		arrayFahrkartenpreise[15] = 9.60;	//ES e ABC 4FK
		arrayFahrkartenpreise[16] = 8.60;	//TK R AB
		arrayFahrkartenpreise[17] = 9.00;	//TK R BC
		arrayFahrkartenpreise[18] = 9.60;	//TK R ABC
		arrayFahrkartenpreise[19] = 5.50;	//TK e AB
		arrayFahrkartenpreise[20] = 5.80;	//TK e BC
		arrayFahrkartenpreise[21] = 6.00;	//TK e ABC
		arrayFahrkartenpreise[22] = 23.50;	//KG e ABC
		arrayFahrkartenpreise[23] = 24.30;	//KG e ABC
		arrayFahrkartenpreise[24] = 24.90;	//KG e ABC
		arrayFahrkartenpreise[25] = 1.70;	//AF R 
		arrayFahrkartenpreise[26] = 1.30;	//AF e 
		return arrayFahrkartenpreise[preis];							//Übergabe des Fahrkartenpreises als Integer zur Berechnung und Ausgabe außerhalb der Methode
	}

	public static double fahrkartenBezahlen(double preisGesamt) 		//Methode zum Abbezahlen des Gesamtpreises mittels Münzeinwurf (Übergabe Variable Gesamtsumme an Methode)
	{ 	
		double bezahltBetrag = 0.0; 									//Variable für bereits bezahlte Summe
		double rückgabe = 0;											//Variable für die spätere Übergabe des Rückgabebetrages
		boolean richtigeEingabe = false;								//Boolean Variable zum Spei
		double[] gültigeMünzen = new double[7];
		gültigeMünzen[0] = 0.05;
		gültigeMünzen[1] = 0.1;
		gültigeMünzen[2] = 0.2;
		gültigeMünzen[3] = 0.5;
		gültigeMünzen[4] = 1;
		gültigeMünzen[5] = 2;

		Scanner tastatur = new Scanner(System.in);						//Initialisierung Scanner 'tastatur'
	       while(bezahltBetrag < preisGesamt)							//Schleife, solange der Ticketpreis nicht bezahlt wurde
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (preisGesamt - bezahltBetrag)); //Ausgabe Information der Restsumme
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): "); //Zahlungsbedingungen, noch keine Regel für Nichteinhaltung programmiert.
	    	   double bezahlteMünzen = tastatur.nextDouble();				//Eingabe/Einwurf der Münzen
	    	   richtigeEingabe = false;
	    	   for (int x = 0; x <=6 ;x++)							
	    	   {
	    		   if (gültigeMünzen[x] == bezahlteMünzen) 
	    		   {
	    			   richtigeEingabe = true;
	    		   }
	    	   }
	    	   while (richtigeEingabe == false)
	    	   {
	    		   for (int i = 0; i <=6 ;i++)
		    	   {
		    		   if (gültigeMünzen[i] == bezahlteMünzen) 
		    		   {
		    			   richtigeEingabe = true;
		    		   }
		    	   }
	    		   System.out.println("Geldstück nicht erkannt oder ungültige Eingabe.");
		    	   bezahlteMünzen = tastatur.nextDouble();				//Eingabe/Einwurf der Münzen
	    	   }
	    	   bezahltBetrag += bezahlteMünzen;								//Summieren der Münzen um gezahlten Betrag aufzustocken
	    	   rückgabe = bezahltBetrag - preisGesamt;						//Berechnung Rückgabewert durch subtrahieren des Gesamtpreises vom eingezahlten Wert
	       }
	       return rückgabe;													//Übergabe des Rückgabewertes zur späteren Verwendung außerhalb der Methode
	}

	public static void fahrkartenAusgeben(int lag) 							//Methode zur Druckprozesssimulation (ohne Übergabeparameter)
	{						
	       // Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");			//Ausgabe Informationstext, Drucken Fahrschein Start
	       for (int i = 0; i < 8; i++) 									// Variable			Datentyp Integer
	       {
	          System.out.print("=");
	          warte(lag);												//Wartezeit 250ms
	       }
	       
	       System.out.println("\n\n");
	}
	
	public static void warte(int millisekunden) 						//Methode warte(), die als Parameter eine Zeit in Milisekunden besitzt und nichts zurückliefert.
	{					
		 try {
				Thread.sleep(millisekunden);							//Wartesimulation 250ms pro Schleifendurchlauf
			} catch (InterruptedException e) {							//Auffangen von Fehlern mit catch
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public static void rueckgeldAusgeben(double rückgabe) 				//Rückgeld - Methode, wenn nicht passend gezahlt wurde (Übergabe Variable Rückgeld)
	{		
		int[] arrayMünzenBetrag = new int[8];
		String[] arrayMünzenArt = new String[8];						//1 = Euro, 0 = Cent
 	   	int anzahl = 0;
 	   	int euro = 0;
 	   	int cent = 0;
 	   	
		if(rückgabe >= 0.0)													//wenn Rückgeld nicht ausgeglichen: Auswahl der Rückgabe in entsprechenden Münzen
	       {
	    	   System.out.printf("%s%.2f%s", "Der Rückgabebetrag in Höhe von " , rückgabe , " EURO");
	    	   System.out.println(" wird in folgenden Münzen ausgezahlt:");	//von groß nach kleinen Münzen
	    	   
	    	   
	           while(rückgabe >= 2.0) // 2 EURO-Münzen
	           {
	        	   muenzeAusgeben(2, " EURO"); 
	        	   rückgabe -= 2.00;
	        	   arrayMünzenBetrag[anzahl] = 2;
	        	   arrayMünzenArt[anzahl] = "Euro";
	        	   anzahl++;
	        	   euro++;
	           }
	           while(rückgabe >= 1.0) // 1 EURO-Münzen
	           {
	        	  muenzeAusgeben(1, " EURO");;
	        	  rückgabe -= 1.00;
	        	  arrayMünzenBetrag[anzahl] = 1;
	        	  arrayMünzenArt[anzahl] = "Euro";
	        	  anzahl++;
	        	  euro++;
	           }
	           while(rückgabe >= 0.5) // 50 CENT-Münzen
	           {
	        	  muenzeAusgeben(50, "CENT");
	        	  rückgabe -= 0.50;
	        	  arrayMünzenBetrag[anzahl] = 50;
	        	  arrayMünzenArt[anzahl] = "Cent";
	        	  anzahl++;
	        	  cent++;
	           }
	           while(rückgabe >= 0.20) // 20 CENT-Münzen
	           {
	        	   muenzeAusgeben(20, "CENT");
	        	  rückgabe -= 0.20;
	        	  arrayMünzenBetrag[anzahl] = 20;
	        	  arrayMünzenArt[anzahl] = "Cent";
	        	  anzahl++;
	        	  cent++;
	           }
	           while(rückgabe >= 0.10) // 10 CENT-Münzen
	           {
	        	   muenzeAusgeben(10, "CENT");
	        	   arrayMünzenBetrag[anzahl] = 10;
	        	   arrayMünzenArt[anzahl] = "Cent";
	        	  rückgabe -= 0.10;
	        	  anzahl++;
	        	  cent++;
	           }
	           while(rückgabe >= 0.04)// 5 CENT-Münzen
	           {
	        	   muenzeAusgeben(5, "CENT");
	        	   arrayMünzenBetrag[anzahl] = 5;
	        	   arrayMünzenArt[anzahl] = "Cent";
	        	  rückgabe -= 0.05;
	        	  anzahl++;
	        	  cent++;
	           }
	       }
		if(anzahl == 1)
			rueckgeldAusgabe(anzahl, arrayMünzenArt[anzahl -1], arrayMünzenArt[anzahl -1], arrayMünzenArt[anzahl -1], arrayMünzenBetrag[anzahl -1], arrayMünzenBetrag[anzahl -1], arrayMünzenBetrag[anzahl -1]);
		else if(anzahl == 2)
			rueckgeldAusgabe(anzahl, arrayMünzenArt[anzahl -2], arrayMünzenArt[anzahl -1], arrayMünzenArt[anzahl], arrayMünzenBetrag[anzahl -2], arrayMünzenBetrag[anzahl -1], arrayMünzenBetrag[anzahl]);
		else if(anzahl == 3)
			rueckgeldAusgabe(anzahl, arrayMünzenArt[anzahl -3], arrayMünzenArt[anzahl -2], arrayMünzenArt[anzahl -1], arrayMünzenBetrag[anzahl -3], arrayMünzenBetrag[anzahl -2], arrayMünzenBetrag[anzahl -1]);
		else if(anzahl == 4)
		{
			rueckgeldAusgabe(anzahl, arrayMünzenArt[anzahl -4], arrayMünzenArt[anzahl -3], arrayMünzenArt[anzahl -2], arrayMünzenBetrag[anzahl -4], arrayMünzenBetrag[anzahl -3], arrayMünzenBetrag[anzahl -2]);
			System.out.println();
			rueckgeldAusgabe(anzahl, arrayMünzenArt[anzahl -3], arrayMünzenArt[anzahl -2], arrayMünzenArt[anzahl -1], arrayMünzenBetrag[anzahl -3], arrayMünzenBetrag[anzahl -2], arrayMünzenBetrag[anzahl -1]);
		}
		else if(anzahl == 5)
		{
			rueckgeldAusgabe(anzahl, arrayMünzenArt[anzahl -5], arrayMünzenArt[anzahl -4], arrayMünzenArt[anzahl -3], arrayMünzenBetrag[anzahl -5], arrayMünzenBetrag[anzahl -4], arrayMünzenBetrag[anzahl -3]);
			System.out.println();
			rueckgeldAusgabe(anzahl, arrayMünzenArt[anzahl -4], arrayMünzenArt[anzahl -3], arrayMünzenArt[anzahl -2], arrayMünzenBetrag[anzahl -4], arrayMünzenBetrag[anzahl -3], arrayMünzenBetrag[anzahl -2]);
		}
		else if(anzahl == 6)
		{
			rueckgeldAusgabe(anzahl, arrayMünzenArt[anzahl -6], arrayMünzenArt[anzahl -5], arrayMünzenArt[anzahl -4], arrayMünzenBetrag[anzahl -6], arrayMünzenBetrag[anzahl -5], arrayMünzenBetrag[anzahl -4]);
			System.out.println();
			rueckgeldAusgabe(anzahl, arrayMünzenArt[anzahl -5], arrayMünzenArt[anzahl -4], arrayMünzenArt[anzahl -3], arrayMünzenBetrag[anzahl -5], arrayMünzenBetrag[anzahl -4], arrayMünzenBetrag[anzahl -3]);
		}
	}
	public static void LogoBVG() 	//BVG-Logo 
	{
		System.out.println(
 
				"      _____   _____ \n" + 
				"     | _ ) \\ / / __|\n" + 
				"     | _ \\\\ V / (_ |\n" + 
				"     |___/ \\_/ \\___|\n" + 
				"  #WEIL WIR DICH LIEBEN.\n");
	}
	
	public static void trenner()	//optischer Trenner Methode
	{
		for (int i = 1; i <= 70; i++) {
			System.out.print("="); 	//optischer Trenner mit Strichen
		}
		System.out.println("");
	}
	
	public static void muenzeAusgeben(int betrag, String einheit)	 	//Methode Münzbetrag Ausgabe mit Übergabeparameter (betrag und einheit)
	{ 
		System.out.printf("%d %s\n", betrag , einheit);
	}
	public static void rueckgeldAusgabe(int anzahl, String typ1, String typ2, String typ3, int betrag1, int betrag2, int betrag3)
	{
		String c ="#";
		String x ="€";
		switch (anzahl) {
		case 1:
			if (typ1 == "Euro")
				System.out.printf("%4s%2s%2s\n%2s%8s\n%s%5d%5s\n%s%7s%3s\n%2s%8s\n%4s%2s%2s\n", x , x , x , x , x , x , betrag1 , x , x , typ1 , x , x , x , x , x , x);
			else
				System.out.printf("%4s%2s%2s\n%2s%8s\n%s%6d%4s\n%s%7s%3s\n%2s%8s\n%4s%2s%2s\n", c , c , c , c , c , c , betrag1 , c , c , typ1 , c , c , c , c , c , c);
			break;
		case 2:
			//1. Zeile
			if (typ1 == "Euro")
				System.out.printf("%4s%2s%2s", x , x , x);
			else
				System.out.printf("%4s%2s%2s", c , c , c);
			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s\n", x , x , x);
			else
				System.out.printf("%8s%2s%2s\n", c , c , c);
			
			//2. Zeile
			if (typ1 == "Euro")
				System.out.printf("%2s%8s", x , x );
			else
				System.out.printf("%2s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s\n", x , x );
			else
				System.out.printf("%4s%8s\n", c , c );
			
			//3. Zeile
			if (typ1 == "Euro")
				System.out.printf("%s%5d%5s", x , betrag1 , x);
			else
				System.out.printf("%s%6d%4s", c , betrag1 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%5d%5s\n", x , betrag2 , x);
			else
				System.out.printf("%2s%6d%4s\n", c , betrag2 , c);
			
			//4. Zeile
			if (typ1 == "Euro")
				System.out.printf("%s%7s%3s", x , typ1 , x);
			else
				System.out.printf("%s%7s%3s", c , typ1 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%7s%3s\n", x , typ2 , x);
			else
				System.out.printf("%2s%7s%3s\n", c , typ2 , c);
			
			//5. Zeile
			if (typ1 == "Euro")
				System.out.printf("%2s%8s", x , x );
			else
				System.out.printf("%2s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s\n", x , x );
			else
				System.out.printf("%4s%8s\n", c , c );
			
			//6. Zeile
			if (typ1 == "Euro")
				System.out.printf("%4s%2s%2s", x , x , x);
			else
				System.out.printf("%4s%2s%2s", c , c , c);
			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s\n", x , x , x);
			else
				System.out.printf("%8s%2s%2s\n", c , c , c);
			break;
			
		case 3:
			//1. Zeile
			if (typ1 == "Euro")
				System.out.printf("%4s%2s%2s", x , x , x);
			else
				System.out.printf("%4s%2s%2s", c , c , c);
			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s", x , x , x);
			else
				System.out.printf("%8s%2s%2s", c , c , c);
			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s\n", x , x , x);
			else
				System.out.printf("%8s%2s%2s\n", c , c , c);
			
			//2. Zeile
			if (typ1 == "Euro")
				System.out.printf("%2s%8s", x , x );
			else
				System.out.printf("%2s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s", x , x );
			else
				System.out.printf("%4s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s\n", x , x );
			else
				System.out.printf("%4s%8s\n", c , c );
			
			//3. Zeile
			if (typ1 == "Euro")
				System.out.printf("%s%5d%5s", x , betrag1 , x);
			else
				System.out.printf("%s%6d%4s", c , betrag1 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%5d%5s", x , betrag2 , x);
			else
				System.out.printf("%2s%6d%4s", c , betrag2 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%5d%5s\n", x , betrag2 , x);
			else
				System.out.printf("%2s%6d%4s\n", c , betrag2 , c);
			
			//4. Zeile
			if (typ1 == "Euro")
				System.out.printf("%s%7s%3s", x , typ1 , x);
			else
				System.out.printf("%s%7s%3s", c , typ1 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%7s%3s", x , typ2 , x);
			else
				System.out.printf("%2s%7s%3s", c , typ2 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%7s%3s\n", x , typ2 , x);
			else
				System.out.printf("%2s%7s%3s\n", c , typ2 , c);
			
			//5. Zeile
			if (typ1 == "Euro")
				System.out.printf("%2s%8s", x , x );
			else
				System.out.printf("%2s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s", x , x );
			else
				System.out.printf("%4s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s\n", x , x );
			else
				System.out.printf("%4s%8s\n", c , c );
			
			//6. Zeile
			if (typ1 == "Euro")
				System.out.printf("%4s%2s%2s", x , x , x);
			else
				System.out.printf("%4s%2s%2s", c , c , c);
			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s", x , x , x);
			else
				System.out.printf("%8s%2s%2s", c , c , c);

			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s\n", x , x , x);
			else
				System.out.printf("%8s%2s%2s\n", c , c , c);
			break;
		}
}
}