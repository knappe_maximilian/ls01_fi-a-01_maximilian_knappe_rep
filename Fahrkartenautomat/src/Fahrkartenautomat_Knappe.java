import java.util.Scanner;

public class Fahrkartenautomat_Knappe {

	public static void main(String[] args) {

		int lag = 1;
		Scanner tastatur = new Scanner(System.in);
		String fahrkartentyp = "";
		int fahrkartenanzahl = 1;
		String tarif = "Regeltarif";
		double restbetrag = 0;
		
		LogoBVG();
		Strich(55);
		MenuArt(lag);
		Eingabe();
		fahrkartentyp = tastatur.next();
		switch(fahrkartentyp) {
		case "T":
			fahrkartentyp = Zonenwahl(fahrkartentyp);
			break;
		case "E":
			fahrkartentyp = Zonenwahl(fahrkartentyp);
			break;
		}
		
		fahrkartentyp = Kartentyp(fahrkartentyp);
		Strich(75);
		
		if (fahrkartentyp == "Andere")
			AndereTyp();
		else
			MenuBez(fahrkartentyp, fahrkartenanzahl, tarif, restbetrag);
		
		Eingabe();

	}
	
	public static String Zonenwahl(String fahrkartentyp) {
		Scanner tastatur = new Scanner(System.in);
		Strich(35);
		String trenner = "_____";
		System.out.printf("\n\n%34s","W�hlen Sie die gew�nschte Tarifzone");
		System.out.printf("\n%10s%10s%10s", trenner , trenner , trenner);
		System.out.printf("\n%5s%6s%4s%6s%4s%6s", "/" , "\\" , "/" , "\\" , "/" , "\\");
		System.out.printf("\n%4s%5s%3s%2s%5s%3s%2s%5s%3s", "/" , "---", "\\" , "/" , "---", "\\" , "/" , "---" , "\\");
		System.out.printf("\n%3s%6s%4s%6s%4s%6s%4s", "X" , "A B" , "X" , "B C" , "X" , "ABC" , "X");
		System.out.printf("\n%4s%5s%3s%2s%5s%3s%2s%5s%3s", "\\" , "---", "/" , "\\" , "---", "/" , "\\" , "---" , "/");
		System.out.printf("\n%11s%10s%10s", "\\_____/" , "\\_____/" , "\\_____/");
		
		Eingabe();
		String eingabe = tastatur.next();
		switch(fahrkartentyp) {
		case "Einzelfahrschein":
			switch(eingabe) {
			case "AB":
				fahrkartentyp = "Einzelfahrschein_AB";
			break;
			case "BC":
				fahrkartentyp = "Einzelfahrschein_BC";
			break;
			case "ABC":
				fahrkartentyp = "Einzelfahrschein_ABC";
			break;
			}
		case "Tageskarte":
			switch(eingabe) {
			case "AB":
				fahrkartentyp = "Tageskarte_AB";
			break;
			case "BC":
				fahrkartentyp = "Tageskarte_BC";
			break;
			case "ABC":
				fahrkartentyp = "Tageskarte_ABC";
			break;
			}
			
			break;
		}
		
		return fahrkartentyp;
	}
	
	public static void MenuBez(String typ, int anzahl, String tarif, double restbetrag) {
		String trenner = " ---------------------------";
		String regelT = "Regeltarif[R]";
		String ermT = "Erm��igungstarif[E]";
		String anzF = "mehrere Fahrscheine[M]";
		double preis = Kartenpreis (typ);
		System.out.println("\n\n " + typ);
		System.out.printf("%74s\n%47s%11s%15s\n%74s\n%47s%17s%9s\n%74s", trenner, "|", regelT, "|" , trenner , "|" , ermT, "|" , trenner);
		System.out.printf("\n %d%15s\n", anzahl , tarif);
		System.out.printf("%74s\n%47s%20s%6s\n%74s\n",trenner, "|", anzF, "|" , trenner);
		System.out.printf(" %-25s%s %-25.2f\n","Gesamtbetrag", "EUR" ,preis);
		Strich(75);
		System.out.printf("\n\n %s%45s%5.2f", "Noch zu zahlender Betrag", "EUR" ,restbetrag);
	}
	
	public static double Kartenpreis(String typ){
		double preis = 0;
		switch(typ) {
		
		case "Einzelfahrausweis_AB":
			preis = 2.90;
			break;
			
		case "Einzelfahrausweis_BC":
			preis = 3.30;
			break;
			
		case "Einzelfahrausweis_ABC":
			preis = 3.60;
			break;
			
		case "Tageskarte_AB":
			preis = 8.60;
			break;
			
		case "Tageskarte_CB":
			preis = 9.00;
			break;	
			
		case "Tageskarte_ABC":
			preis = 9.60;
			break;	
			
		case "Kurzstrecke":
			preis = 1.40;
			break;
		}
		
		return preis;
	}
	
	public static void AndereTyp(){
		System.out.printf("%s","Test");
	}
	public static String Kartentyp(String typ){
		switch (typ) {
			case "K":
				typ = "Kurzstrecke";
				break;
		
			case "EAB":
				typ = "Einzelfahrausweis_AB";
				break;
			case "EBC":
				typ = "Einzelfahrausweis_BC";
				break;
			case "EABC":
				typ = "Einzelfahrausweis_ABC";
				break;
			
			case "TAB":
				typ = "Tageskarte_AB";
				break;
			case "TBC":
				typ = "Tageskarte_BC";
				break;
			case "TABC":
				typ = "Tageskarte_ABC";
				break;
				
			case "A":
				typ = "Andere";
				break;
	}
		return typ;
	}
	public static void MenuArt(int lag) {
		String trenner = " -----------------------------------------------------";
		String zone = "---Berlin----";
		String za = "-A-";
		String zb = "-B-";
		String zc = "-C-";
		String ezf = "Einzelfahrschein[E]";
		String tgk = "Tageskarte[T]";
		warte(lag);
	System.out.printf(" \n\n%46s\n\n%s%-19s%-36s%s","W�hlen Sie den gew�nschten Fahrschein" , trenner ,"\n|","Kurzstrecke[K]", "|\n" + trenner);
		warte(lag);
	System.out.printf(" \n|%15s%40s%-3s|%s|%s|%25s%15s  |\n|%54s\n%s" , zone , "|\n", "|", za , zb , ezf , tgk , "|", trenner);
		warte(lag);
	System.out.printf(" \n|%15s%40s%-7s|%s|%s|%21s%15s  |\n|%54s\n%s" , zone , "|\n", "|", zb , zc , ezf , tgk , "|", trenner);
		warte(lag);
	System.out.printf(" \n|%15s%40s%-3s|%s|%s|%s|%21s%15s  |\n|%54s\n%s" , zone , "|\n", "|", za , zb , zc , ezf , tgk , "|", trenner);
		warte(lag);
	System.out.printf(" %-19s%-36s%s","\n|","Andere Fahrscheine[A]...", "|\n" + trenner);
	}
	
	public static void LogoBVG() {
		System.out.println(
				"\n               ######     #     #     #####  \r\n" + 
				"               #     #    #     #    #     # \r\n" + 
				"               #     #    #     #    #       \r\n" + 
				"               ######     #     #    #  #### \r\n" + 
				"               #     #     #   #     #     # \r\n" + 
				"               #     #      # #      #     # \r\n" + 
				"               ######        #        ##### " + 
				"\n\n                  #WEIL WIR DICH LIEBEN.");
	}
	
	public static void Strich(int anz) {
		for (int i = 1; i <=anz;i++) {
		System.out.print("_");}
	}
	
	public static void Eingabe() {
		System.out.print("\n\n Ihre Eingabe: ");
	}
	
	public static void Beenden() {
		System.exit( 0 );
	}
	
	public static void warte(int millisekunde) {
		try
		{
		    Thread.sleep(millisekunde);
		}
		catch(InterruptedException ex)
		{
		    Thread.currentThread().interrupt();
		}
	}
	
	public static void rueckgeldAusgabe(int anzahl, String typ1, String typ2, String typ3, int betrag1, int betrag2, int betrag3){
		String c ="#";
		String x ="�";
		switch (anzahl) {
		case 1:
			if (typ1 == "Euro")
				System.out.printf("%4s%2s%2s\n%2s%8s\n%s%5d%5s\n%s%7s%3s\n%2s%8s\n%4s%2s%2s\n", x , x , x , x , x , x , betrag1 , x , x , typ1 , x , x , x , x , x , x);
			else
				System.out.printf("%4s%2s%2s\n%2s%8s\n%s%6d%4s\n%s%7s%3s\n%2s%8s\n%4s%2s%2s\n", x , x , x , x , x , x , betrag1 , x , x , typ1 , x , x , x , x , x , x);
			break;
		case 2:
			//1. Zeile
			if (typ1 == "Euro")
				System.out.printf("%4s%2s%2s", x , x , x);
			else
				System.out.printf("%4s%2s%2s", c , c , c);
			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s\n", x , x , x);
			else
				System.out.printf("%8s%2s%2s\n", c , c , c);
			
			//2. Zeile
			if (typ1 == "Euro")
				System.out.printf("%2s%8s", x , x );
			else
				System.out.printf("%2s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s\n", x , x );
			else
				System.out.printf("%4s%8s\n", c , c );
			
			//3. Zeile
			if (typ1 == "Euro")
				System.out.printf("%s%5d%5s", x , betrag1 , x);
			else
				System.out.printf("%s%6d%4s", c , betrag1 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%5d%5s\n", x , betrag2 , x);
			else
				System.out.printf("%2s%6d%4s\n", c , betrag2 , c);
			
			//4. Zeile
			if (typ1 == "Euro")
				System.out.printf("%s%7s%3s", x , typ1 , x);
			else
				System.out.printf("%s%7s%3s", c , typ1 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%7s%3s\n", x , typ2 , x);
			else
				System.out.printf("%2s%7s%3s\n", c , typ2 , c);
			
			//5. Zeile
			if (typ1 == "Euro")
				System.out.printf("%2s%8s", x , x );
			else
				System.out.printf("%2s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s\n", x , x );
			else
				System.out.printf("%4s%8s\n", c , c );
			
			//6. Zeile
			if (typ1 == "Euro")
				System.out.printf("%4s%2s%2s", x , x , x);
			else
				System.out.printf("%4s%2s%2s", c , c , c);
			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s\n", x , x , x);
			else
				System.out.printf("%8s%2s%2s\n", c , c , c);
			break;
			
		case 3:
			//1. Zeile
			if (typ1 == "Euro")
				System.out.printf("%4s%2s%2s", x , x , x);
			else
				System.out.printf("%4s%2s%2s", c , c , c);
			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s", x , x , x);
			else
				System.out.printf("%8s%2s%2s", c , c , c);
			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s\n", x , x , x);
			else
				System.out.printf("%8s%2s%2s\n", c , c , c);
			
			//2. Zeile
			if (typ1 == "Euro")
				System.out.printf("%2s%8s", x , x );
			else
				System.out.printf("%2s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s", x , x );
			else
				System.out.printf("%4s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s\n", x , x );
			else
				System.out.printf("%4s%8s\n", c , c );
			
			//3. Zeile
			if (typ1 == "Euro")
				System.out.printf("%s%5d%5s", x , betrag1 , x);
			else
				System.out.printf("%s%6d%4s", c , betrag1 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%5d%5s", x , betrag2 , x);
			else
				System.out.printf("%2s%6d%4s", c , betrag2 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%5d%5s\n", x , betrag2 , x);
			else
				System.out.printf("%2s%6d%4s\n", c , betrag2 , c);
			
			//4. Zeile
			if (typ1 == "Euro")
				System.out.printf("%s%7s%3s", x , typ1 , x);
			else
				System.out.printf("%s%7s%3s", c , typ1 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%7s%3s", x , typ2 , x);
			else
				System.out.printf("%2s%7s%3s", c , typ2 , c);
			if (typ2 == "Euro")
				System.out.printf("%2s%7s%3s\n", x , typ2 , x);
			else
				System.out.printf("%2s%7s%3s\n", c , typ2 , c);
			
			//5. Zeile
			if (typ1 == "Euro")
				System.out.printf("%2s%8s", x , x );
			else
				System.out.printf("%2s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s", x , x );
			else
				System.out.printf("%4s%8s", c , c );
			if (typ2 == "Euro")
				System.out.printf("%4s%8s\n", x , x );
			else
				System.out.printf("%4s%8s\n", c , c );
			
			//6. Zeile
			if (typ1 == "Euro")
				System.out.printf("%4s%2s%2s", x , x , x);
			else
				System.out.printf("%4s%2s%2s", c , c , c);
			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s", x , x , x);
			else
				System.out.printf("%8s%2s%2s", c , c , c);

			if (typ2 == "Euro")
				System.out.printf("%8s%2s%2s\n", x , x , x);
			else
				System.out.printf("%8s%2s%2s\n", c , c , c);
			break;
		}
	}
	
}
