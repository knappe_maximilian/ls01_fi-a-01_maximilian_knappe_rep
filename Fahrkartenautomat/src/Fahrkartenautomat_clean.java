import java.util.Scanner;

public class Fahrkartenautomat_clean {
	
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double preisTicket;
		int anzahlTickets;
		double preisGesamt;
		System.out.printf("Ticketpreis (EURO): ");
		preisTicket = tastatur.nextDouble();
		System.out.printf("Wie viel Tickets wollen Sie erwerben? ");
		anzahlTickets = tastatur.nextInt();
		preisGesamt = preisTicket * anzahlTickets;
		return preisGesamt;
	}
	public static double fahrkartenBezahlen(double preisGesamt) {
		double bezahltBetrag = 0.0;
		double r�ckgabe = 0;
		Scanner tastatur = new Scanner(System.in);
	       while(bezahltBetrag < preisGesamt)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (preisGesamt - bezahltBetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	  double bezahlteM�nzen = tastatur.nextDouble();
	    	  bezahltBetrag += bezahlteM�nzen;
	    	  r�ckgabe = bezahltBetrag - preisGesamt;
	       }
	       return r�ckgabe;
	}
	public static void fahrkartenAusgeben() {
	       // Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++) 		// Variable i,							Datentyp Integer
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	public static void rueckgeldAusgeben(double r�ckgabe) {

		if(r�ckgabe >= 0.0)
	       {
	    	   System.out.printf("%s%.2f%s", "Der R�ckgabebetrag in H�he von " , r�ckgabe , " EURO");
	    	   System.out.println(" wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabe >= 2.0) //Automat nimmt maximal 2� St�cke
	           {
	        	  System.out.println("2 EURO"); // 2 EURO-M�nzen
	        	  r�ckgabe -= 2.00;
	           }
	           while(r�ckgabe >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
	        	  r�ckgabe -= 1.00;
	           }
	           while(r�ckgabe >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
	        	  r�ckgabe -= 0.50;
	           }
	           while(r�ckgabe >= 0.20) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	        	  r�ckgabe -= 0.20;
	           }
	           while(r�ckgabe >= 0.10) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
	        	  r�ckgabe -= 0.10;
	           }
	           while(r�ckgabe >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	        	  r�ckgabe -= 0.05;
	           }
	       }
	}
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
	      
	       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	       double r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	       fahrkartenAusgeben();
	       rueckgeldAusgeben(r�ckgabebetrag);

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	}
}

