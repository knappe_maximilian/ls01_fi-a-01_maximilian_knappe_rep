import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.management.StringValueExp;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JTextField;

public class SwingFrame extends JFrame implements ActionListener {

	private JPanel panel;
	private JLabel label, label2;
	private JTable table;
	private JTextField zeit1, zeit2, zeit3, zeit4, zeit5, zeit6, zeit7, zeit8, zeit9, zeit10, endzeit, endzeit2;
	private JButton button1, button2, button3;
	long avTime, stop, start = 0;
	int durchgang = 0;

	private int a, b, c, d = 50;
	long[] wertZeit = new long[10];

	public SwingFrame() {

		setTitle("KlickAIM");
		setSize(700, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		add(panel);
		panel.setLayout(null);
		panel.setBackground(Color.getHSBColor(50, 100, 250));

		button1 = new JButton("A");
		button1.addActionListener(this);
		panel.add(button1);
		button1.setBounds(50, 50, 50, 50);
		button1.setVisible(false);
		button1.setFont(new Font("", 0, 1));
		button1.setBackground(Color.RED);
		
		button2 = new JButton("Start");
		button2.addActionListener(this);
		panel.add(button2);
		button2.setBounds(150, 50, 300, 300);

		label = new JLabel("time(ms)");
		panel.add(label);
		label.setBounds(580, 10, 100, 30);
		label.setFont(new Font("", 0, 25));
		
		label2 = new JLabel("�");
		panel.add(label2);
		label2.setBounds(550, 350, 100, 30);
		label2.setFont(new Font("", 0, 25));

		zeit1 = new JTextField("");
		panel.add(zeit1);
		zeit1.setBounds(580, 50, 100, 30);
		zeit1.setVisible(true);
		zeit1.setFont(new Font("", 0, 25));

		zeit2 = new JTextField("");
		panel.add(zeit2);
		zeit2.setBounds(580, 80, 100, 30);
		zeit2.setVisible(true);
		zeit2.setFont(new Font("", 0, 25));

		zeit3 = new JTextField("");
		panel.add(zeit3);
		zeit3.setBounds(580, 110, 100, 30);
		zeit3.setFont(new Font("", 0, 25));

		zeit4 = new JTextField("");
		panel.add(zeit4);
		zeit4.setBounds(580, 140, 100, 30);
		zeit4.setVisible(true);
		zeit4.setFont(new Font("", 0, 25));

		zeit5 = new JTextField("");
		panel.add(zeit5);
		zeit5.setBounds(580, 170, 100, 30);
		zeit5.setVisible(true);
		zeit5.setFont(new Font("", 0, 25));

		zeit6 = new JTextField("");
		panel.add(zeit6);
		zeit6.setBounds(580, 200, 100, 30);
		zeit6.setVisible(true);
		zeit6.setFont(new Font("", 0, 25));

		zeit7 = new JTextField("");
		panel.add(zeit7);
		zeit7.setBounds(580, 230, 100, 30);
		zeit7.setVisible(true);
		zeit7.setFont(new Font("", 0, 25));

		zeit8 = new JTextField("");
		panel.add(zeit8);
		zeit8.setBounds(580, 260, 100, 30);
		zeit8.setVisible(true);
		zeit8.setFont(new Font("", 0, 25));

		zeit9 = new JTextField("");
		panel.add(zeit9);
		zeit9.setBounds(580, 290, 100, 30);
		zeit9.setVisible(true);
		zeit9.setFont(new Font("", 0, 25));

		zeit10 = new JTextField("");
		panel.add(zeit10);
		zeit10.setBounds(580, 320, 100, 30);
		zeit10.setVisible(true);
		zeit10.setFont(new Font("", 0, 25));

		endzeit = new JTextField("");
		panel.add(endzeit);
		endzeit.setBounds(580, 350, 100, 30);
		endzeit.setVisible(true);
		endzeit.setFont(new Font("", 0, 25));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		switch (cmd) {
		case "Start":
				button1.setVisible(true);
				button2.setVisible(false);
				start = System.currentTimeMillis();
			break;
		case "A":
			a = randomNumber.getRandomNumber(10, 500);
			b = randomNumber.getRandomNumber(10, 500);
			c = randomNumber.getRandomNumber(20, 50);
			d = randomNumber.getRandomNumber(20, 50);
			button1.setBounds(a, b, c, d);

			switch (durchgang) {
			case 0:
				stop = System.currentTimeMillis();
				wertZeit[durchgang] = stop - start;
				zeit1.setText(String.valueOf(wertZeit[durchgang]));
				start = System.currentTimeMillis();
				break;
			case 1:
				stop = System.currentTimeMillis();
				wertZeit[durchgang] = stop - start;
				zeit2.setText(String.valueOf(wertZeit[durchgang]));
				start = System.currentTimeMillis();
				break;
			case 2:
				stop = System.currentTimeMillis();
				wertZeit[durchgang] = stop - start;
				zeit3.setText(String.valueOf(wertZeit[durchgang]));
				start = System.currentTimeMillis();
				break;
			case 3:
				stop = System.currentTimeMillis();
				wertZeit[durchgang] = stop - start;
				zeit4.setText(String.valueOf(wertZeit[durchgang]));
				start = System.currentTimeMillis();
				break;
			case 4:
				stop = System.currentTimeMillis();
				wertZeit[durchgang] = stop - start;
				zeit5.setText(String.valueOf(wertZeit[durchgang]));
				start = System.currentTimeMillis();
				break;
			case 5:
				stop = System.currentTimeMillis();
				wertZeit[durchgang] = stop - start;
				zeit6.setText(String.valueOf(wertZeit[durchgang]));
				start = System.currentTimeMillis();
				break;
			case 6:
				stop = System.currentTimeMillis();
				wertZeit[durchgang] = stop - start;
				zeit7.setText(String.valueOf(wertZeit[durchgang]));
				start = System.currentTimeMillis();
				break;
			case 7:
				stop = System.currentTimeMillis();
				wertZeit[durchgang] = stop - start;
				zeit8.setText(String.valueOf(wertZeit[durchgang]));
				start = System.currentTimeMillis();
				break;
			case 8:
				stop = System.currentTimeMillis();
				wertZeit[durchgang] = stop - start;
				zeit9.setText(String.valueOf(wertZeit[durchgang]));
				start = System.currentTimeMillis();
				break;
			case 9:
				stop = System.currentTimeMillis();
				wertZeit[durchgang] = stop - start;
				zeit10.setText(String.valueOf(wertZeit[durchgang]));
				start = System.currentTimeMillis();
				avTime = (wertZeit[0] + wertZeit[1]+wertZeit[2]+wertZeit[3]+wertZeit[4]+wertZeit[5]+wertZeit[6]+wertZeit[7]+wertZeit[8]+wertZeit[9])/10;
				endzeit.setText(String.valueOf(avTime));
				button1.setVisible(false);
				break;
			}
			durchgang = durchgang +1;
			break;

		default:
			throw new IllegalArgumentException("Unexpexted value: " + cmd);

		}

	}
}