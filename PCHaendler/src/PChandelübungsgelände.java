import java.util.Scanner;																//Klasse Scanner -A simple text scanner which can parse primitive types and strings using regular expressions.

public class PChandel�bungsgel�nde {

	public static void main(String[] args) {											// { geschweifte Klammer er�ffnet Anweisungsblock f�r die Methode
																						// jede Zeile mit ; Semikolon am Ende = eine Anweisung
		//Eingabe																		
																						//Deklaration der Variablen mit Zuweisungoperator "=" initialisiert
		String artikel = liesString ("was m�chten Sie bestellen?");						//Datentyp(String) 	Bezeichner(artikel) 	= Wert (liesString) == Variable
		int anzahl = liesInt ("wie viele?");											//Datentyp(int) 	Bezeichner(anzahl) 		= Wert (liesInt) 	== Variable
		double nettopreis = liesDouble ("Preis?");										//Datentyp(double) 	Bezeichner(nettopreis) 	= Wert (liesDouble) == Variable
		double mwst = liesDouble ("Mwst?");												//Datentyp(double) 	Bezeichner(mwst) 		= Wert (liesDouble) == Variable
		
		//Verarbeiten
		
		double nettogesamtpreis = berechneGesamtnettopreis (anzahl, nettopreis);		//Datentyp(double) Bezeichner(anzahl) = Wert (liesInt) == Variable
		double bruttogesamtpreis = berechneGesamtnettopreis (nettogesamtpreis, mwst);
		
		//Ausgabe
		
		rechnungausgabe (artikel, anzahl, nettopreis, nettogesamtpreis, bruttogesamtpreis, mwst);	//Variable "rechnungausgabe" 

	}																						// } geschweifte Klammer schlie�t Anweisungsblock f�r die Methode
	
	public static String liesString (String text) { 										//Methodenkopf - Aufruf einer Methode - Wert (liesString) mit Datentyp (String)
		Scanner myScanner = new Scanner(System.in);											//Methodenrumpf - this code allows long types to be assigned from entries in (System.in)
		System.out.println (text);															//Methode print gibt die bereits deklarierten Wert (text) aus 
		String str = myScanner.next();														//Wartet auf String, also Wort, Eingabe �ber Konsole 
		return str;																			//next/nextInt/nextDouble =  first skip any input that matches the delimiter pattern, and then attempt to return the next token
	}
	public static int liesInt (String text) {			
		Scanner myScanner = new Scanner(System.in);
		System.out.println (text);
		int zahl = myScanner.nextInt();														//Wartet auf Int, also Zahl, Eingabe �ber Konsole
		return zahl;
	}
	public static double liesDouble (String text) {		
		Scanner myScanner = new Scanner(System.in);											//System.in <-> System.out (System.in is the input stream connected to the console, much as System. out is the output stream connected to the console) 
		System.out.println (text);
		double zahl = myScanner.nextDouble();
		return zahl;																		//return- Anweisung zum Beenden - kehrt zum Aufrufer zur�ck
	}
	public static double berechneGesamtnettopreis (int anzahl, double nettopreis) {			//Methodendeklaration - R�ckgabetyp/ Ergebnistyp "doubl" - Parameterliste "(int, double)"
		return anzahl * nettopreis;															//Methodendeklaratio
	}
	public static double berechneGesamtnettopreis (double nettogesamtpreis, double mwst) {	//Methodendeklaration
		return nettogesamtpreis * (1 + mwst / 100);
	}
	public static void rechnungausgabe (String artikel, int anzahl, double nettopreis, double nettogesamtpreis, double bruttogesamtpreis, double mwst) { 
	System.out.println("\tRechnung");
	System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettopreis, nettogesamtpreis);
	System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	System.out.printf("\n\t\t Auf Wiedersehen");
	}
	
	
}