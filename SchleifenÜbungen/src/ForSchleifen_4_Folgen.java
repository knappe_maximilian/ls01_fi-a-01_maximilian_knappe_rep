
public class ForSchleifen_4_Folgen {

	public static void main(String[] args) {

		System.out.printf("%-31s%-3s","Dreierschritte runterz�hlen", "a)");
		for(int i = 99; i >= 9; i -= 3) 						//Z�hlschleife, jeweils um drei runterrechnen
		{
			if(i == 9) 
				System.out.print(i);
			else 
				System.out.print(i + ", ");
		}
		
		System.out.printf("\n%-31s%-3s","Quadratzahlen bis 400", "b)");
		for(int i = 1; i <= 20; i++) 							//Z�hlschleife, Quadratzahlen bis 400
		{
			if(i == 20) 
				System.out.print(i*i);
			else 
				System.out.print(i*i + ", ");
		}
		
		System.out.printf("\n%-31s%-3s","Zahlen bis 102 in 4er Schritte", "c)");
		for(int i = 2; i <= 102; i += 4) 						//Z�hlschleife, Zahlen bis 102 in 4er Schritte
		{
			if (i == 102) 
				System.out.print(i);
			else 
				System.out.print(i + ", ");
		}
		int wert = 1;
		System.out.printf("\n%-31s%-3s","(2n)^2", "d)");		
		
		for(int i = 1;i < 17; i++){								//Z�hlschleife, Zahlen nach (2n)^2
			System.out.printf("%.0f, ", Math.pow(2*i,2));
			}
		
		System.out.printf("\n%-31s%-3s","2er Potenzen", "e)");	
		for(int i = 2; i <= 32768; i *= 2 ) 					//Z�hlschleife, 2er Potenzen
		{
			
			if(i == 32768) 
				System.out.print(i);
			else 
				System.out.print(i + ", ");
		}
	}
}

