import java.util.Scanner;
public class forSchleife_1_Z�hlen {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 						//Initialisierung Scanner tastatur
		
		System.out.print("Bis zu welcher Zahl soll gez�hlt werden?: ");	//Eingabe Z�hlh�he
		int n = tastatur.nextInt();										//Speichern Z�hlh�he in Variable n
		
		System.out.print("heraufz�hlend:   ");								
		for(int i = 1; i <= n; i++)										//Z�hlschleife, solange bis die Z�hlervariable der Z�hlh�he entspricht
		{
			System.out.print(i + " ");									//momentanen Z�hlerwert ausgeben		
		}
		
		System.out.print("\nherunterz�hlend: ");
		for(int i = 1; i <= n; i++)									//Z�hlschleife, solange bis die Z�hlervariable der Z�hlh�he entspricht
		{
			System.out.print((n - i + 1) + " ");					//H�he abz�glich momentanen Z�hlerwert ausgeben	plus eins um bei n zu starten nicht bei null zu enden			
		}
	}
}
	
