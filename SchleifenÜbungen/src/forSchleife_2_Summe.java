import java.util.Scanner;
public class forSchleife_2_Summe {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 							//Initialisierung Scanner tastatur

		System.out.print("Geben Sie bitte einen begrenzenden Wert ein: ");	//Anfrage Z�hlh�he
		int n = tastatur.nextInt();											//Eingabe Z�hlh�he und Speichern in Variable n
		int a = 0, b = 0, c = 0;											//Variablen zum Speichern der Werte
 
		System.out.println("\nFor Schleife");
		for(int i = 1; i <= n; i++)											//Z�hlschleife bis Z�hlh�he erreicht
		{
			a += i;															//n
			
			if(i % 2 == 0)													//2n
				b += i;
			
			if(i % 2 != 0)													//2n+1
				c += i;
			
		}
		System.out.println("Die Summe f�r A betr�gt: " + a);				//Ausgabe der Werte f�r a, b und c
		System.out.println("Die Summe f�r B betr�gt: " + b);
		System.out.println("Die Summe f�r C betr�gt: " + c);
		
		int d = 0, e = 0, f = 0;											//Variablen zum Speichern der Werte
		int z�hler = 1;														//Z�hlervariable f�r Kopfgesteuerte Schleife
		while(z�hler <= n)													//Kopfgesteuerte Schleife bis Z�hlh�he erreicht
		{
			d += z�hler;													//n
			
			if(z�hler % 2 == 0)												//2n
				e += z�hler;
			
			if(z�hler % 2 != 0)												//2n+1
				f += z�hler;
			z�hler++;														//Erh�hung Z�hler um eins f�r n�chsten Durchlauf
		}
		System.out.println("\nWhile Schleife");
		System.out.println("Die Summe f�r A betr�gt: " + d);				//Ausgabe der Werte f�r a, b und c	
		System.out.println("Die Summe f�r B betr�gt: " + e);
		System.out.println("Die Summe f�r C betr�gt: " + f);
		
	}

}
