
public class forSchleife_5_Einmaleins {

	public static void main(String[] args) {

		//Rahmenformatierung
		System.out.println("Das kleine Einmaleins");
		System.out.println("___________________________________");
		System.out.print("___|_"); 
		
		//Tabellenkopf
			int z = 1;			
			for (int i = 1; i <= 10; i += z) 
			{
				if(i < 10)
				System.out.print(i + "__");
				else
					System.out.print(i + "_");	
			}
			
		//Ausgabe der Zahlenzeilen beginnend bei 1 und endet bei 10 (i + 1 bis 10)
	 for (z = 1; z <=10 ; z += 1) 
	 {
		System.out.println("|");
		if(z < 10)
		System.out.print(z + "__| "); 
		else
			System.out.print(z + "_| "); 
		
	 		for (int i = z; i <= z*10; i += z) 
	 		{
	 			if (i < 10)	//Unterschiedliche Formatierung, damit es einheitlich ist
	 				System.out.print(i + "  "); 
	 			else if (i < 100)
	 				System.out.print(i + " "); 
	 			else
	 				System.out.print(i + ""); 
	 		}
	 }
	 System.out.println("|");	
		}

	}
