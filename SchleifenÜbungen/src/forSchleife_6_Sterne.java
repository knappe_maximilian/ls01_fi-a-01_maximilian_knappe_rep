import java.util.Scanner;
public class forSchleife_6_Sterne {

		public static void main(String[] args) {

			Scanner tastatur = new Scanner(System.in);
			String star = "*";													//Ausgabe Stern in Variable gespeichert
			System.out.println("Wie hoch (Zeilen) soll das Dreieck werden?");	//Bitte um Eingabe der Sternanzahl
			int h�he = tastatur.nextInt();										//Eingabe der Stern Einzahl und speichern in Variable
			
			for (int i = 1; i <= h�he; i++)										//Z�hlchleife von 1 bis h�he
			{
				System.out.println();											//Neue Zeile
				for (int x = 1;x <= i; x++)										//Zeilenausgabe des Sterns
				{
					System.out.print(star);
				}
			}
		}
	}

