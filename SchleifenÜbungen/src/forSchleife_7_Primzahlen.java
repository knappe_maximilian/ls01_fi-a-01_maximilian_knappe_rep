import java.util.Scanner;
public class forSchleife_7_Primzahlen {

		public static void main(String[] args) {
			Scanner tastatur = new Scanner(System.in);
			boolean trefferP;								//Variable mit Wahrheitswert um "Primzahl = ja/nein" zu speichern
			System.out.println("Bis welche Zahl sollen die Primzahlen ausgegeben werden?");
			int grenze = tastatur.nextInt();				//Grenzwert Eingabe
			
			System.out.print("P = { ");
			
			for (int i = 2; i <= grenze; i++) {				//beginnt ab 2, weil 2 keine Primzahl sein kann
				trefferP = true;							//Wahrheitswert wird auf true gesetzt, bei nicht Primzahl sp�ter auf false gesetzt
				for (int x = 2; x < i && trefferP; x++) {   //Wenn keine Primzahl gefunden wurde, dann wird boolean trefferP auf false gesetzt
						
					if ((i % x) == 0) {						//Kontrolle auf Primzahl (nur durch 1 [hier obsolet] und sich selbst teilbar)
						trefferP = false;					//Wenn keine Primzahl
					}
				}
				if (trefferP) {								//Ausgabe der Primzahlen
					System.out.print(i + " ");
				}
			}
			System.out.print("}");
		}
	}
		
