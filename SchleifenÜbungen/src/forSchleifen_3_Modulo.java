
public class forSchleifen_3_Modulo {

	public static void main(String[] args) {

		System.out.println("Diese Zahlen sind Teiler von 7 und 4, aber nicht 5.");	//Ausgabe: Aufgabe des Programms
		
		for(int i = 1; i <= 200; i++) {												//Z�hlschleife f�r das Durchlaufen der Zahlen 1 bis 200
			if ((i%7 == 0) && (i%4 == 0) && (i%5 != 0)) {							//Bedingungen: durch 7 und 4 restlos teilbar, aber nicht durch 5 
				System.out.println(i);												//Wenn Bedingungen gegeben, dann Z�hlerwertausgeben
			}
		}
	}
}

