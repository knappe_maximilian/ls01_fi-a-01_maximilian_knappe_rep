import java.util.Scanner;
public class forSchleifen_9_Treppe {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 								//Initialisierung Scanner tastatur
		
		System.out.print("Treppenh�he/ Anzahl Treppenstufen:");					//Eingabe Treppenh�he
		int h = tastatur.nextInt();												//Speichern Treppenh�he in Variable h
		
		System.out.print("Stufenbreite:");										//Eingabe Stufenbreite
		int b = tastatur.nextInt();												//Speichern Stufenbreite in Variable b

		int gesamtbreite = b * h;												//Berechnung der maximalen Gesamtl�nge einer Treppenstufe
		
		for(int x = 1; x <= h; x++)												//Z�hlschleife f�r jede Treppenstufe
		{
			for(int i = 1; i <= gesamtbreite - (b * x); i++)					//Z�hlschleife f�r Leerstellen der jeweiligen Treppenstufe x
			{																	//dabei gilt Leerstellenl�nge =  Gesamtl�nge einer Treppenstufe - L�nger der Treppenstufen der jeweiligen Treppenstufe x		
				System.out.print(" ");											//Ausgabe Leerstellen
				}
			for(int i = 1; i <= gesamtbreite - (gesamtbreite - (b * x)); i++)	//Z�hlschleife f�r L�nger der Treppenstufen der jeweiligen Treppenstufe x
			{																	//dabei gilt L�nger der Treppenstufen =  Rest Freistellen auf der Treppenstufe x		
				System.out.print("*");											//Ausgabe Treppenstufenstellen		
				}
			System.out.println();												//n�chste Zeile f�r n�chste Treppenstufe x
		}
	}
}
	
