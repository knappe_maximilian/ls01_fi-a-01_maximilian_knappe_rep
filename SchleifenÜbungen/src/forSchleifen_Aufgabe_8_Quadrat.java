import java.util.Scanner;
public class forSchleifen_Aufgabe_8_Quadrat {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 								//Initialisierung Scanner tastatur
		System.out.print("Wert f�r die Seitenl�nge des Quadrates eingeben:");	//Eingabe Seitenl�nge
		int l�nge = tastatur.nextInt();											//Speichern Seitenl�nge in Variable l�nge
		
		for(int i = 1; i <= l�nge; i++)											//Erste Reihe mit l�nge Seitenl�nge
		{
			System.out.print("* ");												//Ausgabe erste Reihe
			for(int y = 1; y <= (l�nge - 2); y++)								//Z�hlschleife f�r die Ausgabe der Reihen darunter
			{																	//-2 Abzug f�r die erste und letzte Reihe
				if(i == l�nge)													//Kontrollstruktur, wenn erste Reihe voll
				{
					System.out.print("\n* ");									//Erster Stern n�chste Reihe
					for(int x = 1; x <= (l�nge - 2); x++)						//Z�hlschleife f�r Leerraum in n�chster Reihe f�llen
					{															//-2 Abzug von l�nge f�r ersten und letzten Stern in einer Reihe
						System.out.print("  ");									//Ausgabe Leerraum
					}	
					System.out.print("*");										//Letzter Stern n�chste Reihe
				}
			}
		}
		System.out.println();
		for(int i = 1; i <= l�nge; i++)											//Z�hlschleife mit Ausgabe f�r die letzte Reihe
		{
			System.out.print("* ");
		}
	}
}
	
