
public class rekursionen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(fak(6));
		System.out.println(Fak2(6));
	}

	public static int fak(int n) {

		if (n < 2)
			return 1;
		else
			return n * fak(n - 1);
	}

    private static int Fak2(int obergrenze)
    {
        int ergebnis= 1;
        int z�hler= 1;
        int max = obergrenze;

        while (z�hler<= max)
        {
            ergebnis= ergebnis* z�hler;
            z�hler= z�hler+ 1;
        }

        return ergebnis;
    }

}