import java.util.Scanner;
public class whileSchleifen_1_Z�hlen {
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in); 						//Initialisierung Scanner tastatur
		
		System.out.print("Bis zu welcher Zahl soll gez�hlt werden?: ");	//Eingabe Z�hlh�he
		int n = tastatur.nextInt();										//Speichern Z�hlh�he in Variable n
		int z�hler = 1;													//Z�hlervariable f�r die Kopfgesteueren Schleifen
		
		System.out.print("heraufz�hlend:   ");								
		while(z�hler <= n)												//Kopfgesteuerte Schleife, solange bis die Z�hlervariable der Z�hlh�he entspricht
		{
			System.out.print(z�hler + " ");								//momentanen Z�hlerwert ausgeben		
			z�hler ++;													//Z�hler um eins erh�hen
		}
		z�hler = 1;
		System.out.print("\nherunterz�hlend: ");
		while(z�hler <= n)												//Kopfgesteuerte Schleife, solange bis die Z�hlervariable der Z�hlh�he entspricht
		{
			System.out.print((n - z�hler + 1) + " ");					//H�he abz�glich momentanen Z�hlerwert ausgeben	plus eins um bei n zu starten nicht bei null zu enden
			z�hler ++;													//Z�hler um eins erh�hen
		}
	}
}
	
