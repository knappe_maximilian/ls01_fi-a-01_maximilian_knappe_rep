import java.util.Scanner;
public class whileSchleifen_2_Fakult�t {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 									//Initialisierung Scanner tastatur
		int z�hler = 1;																//Z�hler f�r die Kopfgesteuerte Schleife
		int summe = 1;
		System.out.print("Von welcher Zahl soll die Fakult�t berechnet werden?: ");	//Eingabe Fakult�t
		int n = tastatur.nextInt();													//Speichern Fakult�t in Variable n

		System.out.print(n + "!= ");												//Ausgabe der Fakult�tszahl
		while (z�hler <= n)															//Kopfgesteuerte Schleife solange bis die H�he der Fakult�tszahl erreicht wird
		{
			summe = summe * z�hler;													//Multiplikation der Summe der vorherigen Zahlen mit dem aktuellen Z�hler in der Schleife
			z�hler++;																//Erh�hung Z�hler um eins
		}
		System.out.println(summe);													//Ausgabe der errechneten Fakult�t
	}
}
	
