import java.util.Scanner;
public class whileSchleifen_3_Quersumme {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 									//Initialisierung Scanner tastatur									
		int summe = 0;
		System.out.print("Von welcher Zahl soll die Quersumme berechnet werden?: ");//Eingabe Quersumme Zahl
		int n = tastatur.nextInt();													//Speichern Quersumme Zahl in Variable n

		System.out.print("Quersumme von " + n +" ist ");							
		while (0 != n) {
			summe = summe + (n % 10);												//Addiert die letzte Stelle der Zahl auf die Summe
			n = n / 10;																//L�schen der letzten Stelle durch Division durch 10
		}
		System.out.print(summe);													//Ausgabe der errechneten Quersumme
	}
}
	
