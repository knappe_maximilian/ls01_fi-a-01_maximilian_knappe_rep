import java.util.Scanner;
public class whileSchleifen_4_Temperaturumrechnung {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 									//Initialisierung Scanner tastatur
		double celsiusWert;
		double fahrenheitWert;
		//Eingabe der Berechnungsparameter (Startwert, Endwert, Schrittweite) und Speichern in drei Variablen (Kommerzahlen)
		System.out.print("Bitte den Startwert in Celsius eingeben: ");
		double start = tastatur.nextInt();																				
		System.out.print("Bitte den Endwert in Celsius eingeben: ");
		double ende = tastatur.nextInt();				
		System.out.print("Bitte die Schrittweite in Grad Celsius eingeben: ");
		double schritt = tastatur.nextInt();													
		System.out.print("===================================================\n\n");
		
		celsiusWert = start;				//�bergabe Startwert an Celsiuswert zum Weiterrechnen
		while(celsiusWert <= ende)			//Kopfschleife, solange der Celsiuswert nicht dem Endwert entspricht
		{
			fahrenheitWert = celsiusWert * 1.8 +32;		//Formel Umrechnung Celius in Fahrenheit
			if (celsiusWert <= -100)					//Formatierung, damit es sch�n ordentlich untereinander angezeigt wird 
				System.out.printf("%.2f%s%10.2f%s", celsiusWert, "�C" , fahrenheitWert , "�F\n");
			else if(celsiusWert <= -10) 
				System.out.printf("%7.2f%s%10.2f%s", celsiusWert, "�C" , fahrenheitWert , "�F\n");
			else if ((celsiusWert < -10) && (celsiusWert < 0))
				System.out.printf("%7.2f%s%10.2f%s", celsiusWert, "�C" , fahrenheitWert , "�F\n");
			else if ((celsiusWert >= 0) && (celsiusWert < 10))
				System.out.printf("%7.2f%s%10.2f%s", celsiusWert, "�C" , fahrenheitWert , "�F\n");
			else if (celsiusWert >= 10)
				System.out.printf("%7.2f%s%10.2f%s", celsiusWert, "�C" , fahrenheitWert , "�F\n");
			else if (celsiusWert >= 100)
				System.out.printf("%6.2f%s%10.2f%s", celsiusWert, "�C" , fahrenheitWert , "�F\n");
			celsiusWert += schritt;					//Erh�hung des Celsiuswertes in Schrittfolge
		} 
		
	}
}
	
