import java.util.Scanner;
public class whileSchleifen_5_Zinseszins {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 		//Initialisierung Scanner tastatur
		int z�hlerJahr = 1;								//Z�hler f�r die momentane Jahreszahl in der Schleife
		double zwischensumme = 0;						//Variable zum Berechnen der Zwischensumme sowie die Ausgabe nach der Berechnung
		
		//Eingabe der Laufzeit, Startkapital und Zinssatz
		System.out.print("Bitte die Laufzeit in Jahren eingeben: ");
		int laufzeit = tastatur.nextInt();		
		
		System.out.print("Bitte das Startkapital in Euro eingeben: ");
		double kapital = tastatur.nextDouble();
		
		System.out.print("Bitte den Zinssatz in % eingeben: ");
		double zinssatz = tastatur.nextDouble();
		
		//Verarbeitung
		zwischensumme = kapital;						//�bergabe Startkapital an zwischensumme
		while(z�hlerJahr <= laufzeit)					//Kopfgesteuerte Schleife solange Ende der Laufzeit nicht erreicht
		{
			zwischensumme = zwischensumme + (zwischensumme * zinssatz / 100);	//Berechnung Zinseszins und �bergabe Wert an zwischensumme f�r n�chstes Jahr
			z�hlerJahr++;														//Erh�hung Jahreszahl
		}
		//Ausgabe des Startkapitals und des Endkapitals
		System.out.println("===================================");
		System.out.printf("%s%.2f%s%s%.2f%s","Eingezahltes Kapital: ", kapital , " Euro\n" , "Ausgezahltes Kapital: ", zwischensumme  , " Euro" );
	}
}
	
