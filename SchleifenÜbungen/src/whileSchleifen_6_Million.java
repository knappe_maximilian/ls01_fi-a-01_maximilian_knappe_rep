import java.util.Scanner;
public class whileSchleifen_6_Million {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 		//Initialisierung Scanner tastatur
		int z�hlerJahr = 1;								//Z�hler f�r die momentane Jahreszahl in der Schleife
		
		//Eingabe Startkapital und Zinssatz	
		System.out.print("Bitte das Startkapital in Euro eingeben: ");
		double kapital = tastatur.nextDouble();
		
		System.out.print("Bitte den Zinssatz in % eingeben: ");
		double zinssatz = tastatur.nextDouble();
		
		//Verarbeitung
		while(kapital <= 1000000)					//Kopfgesteuerte Schleife bis Zwischensumme eine Million ist
		{
			kapital = kapital + (kapital * zinssatz / 100);	//Berechnung Zinseszins und �bergabe Wert an zwischensumme f�r n�chstes Jahr
			z�hlerJahr++;														//Erh�hung Jahreszahl
		}
		//Ausgabe der Jahre
		System.out.println("===================================");
		System.out.printf("%s%d%s","Sie sind nach ", z�hlerJahr , " Jahren Million�r.");
	}
}
	
