import java.util.Scanner;
public class whileSchleifen_7_Wettlauf {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 		//Initialisierung Scanner tastatur
		int zeit = 1;
		System.out.println("DAS GROSSE RENNEN!");
		System.out.println("========================");
		System.out.println("Wie schnell ist der erste L�ufer in m/s?"); 	//Eingabe Geschwindigkeit L�ufer A in m/s
		double vA = tastatur.nextDouble();	
		double mA = 0;
		System.out.println("Wie schnell ist der zweite L�ufer in m/s?");	//Eingabe Geschwindigkeit L�ufer B in m/s
		double vB = tastatur.nextDouble();		
		double mB = 0;
		System.out.println("Wie lang ist die Rennstrecke in m?");			//Eingabe Rennstreckenl�nge in m
		int l�ngeRennstrecke = tastatur.nextInt();
		
		System.out.printf("\n%s %s %s%s","Zeit","SprinterA","SprinterB\n", "========================\n"); //Tabellenkopf
		while((mA <= l�ngeRennstrecke) && (mB <= l�ngeRennstrecke)) 		//Kopfschleife, solange keiner der L�ufer im Ziel
		{
			mA = mA + vA;		//Aufsummieren der zur�ckgelegten Meter nach xZeit					
			mB = mB + vB;		//Aufsummieren der zur�ckgelegten Meter nach xZeit
			
			//Ausgabe der Werte in formatierter Form
			if(zeit < 10)
				System.out.printf("%3ds%9.1fm%9.1fm\n", zeit, mA, mB);
			 else if(zeit < 100)
				System.out.printf("%3ds%9.1fm%9.1fm\n", zeit, mA, mB);
			 else if(zeit < 1000)
				System.out.printf("%3ds%9.1fm%9.1fm\n", zeit, mA, mB);
			zeit++;			//Z�hler um eins erh�hen
		}
		//Ausgabe Rennresultat
		System.out.println("========================");
		System.out.println("Resultat:");
		if(mA > l�ngeRennstrecke)
			System.out.println("L�ufer A gewinnt nach " + (zeit - 1) + "s.");
		else if (mB > l�ngeRennstrecke)
			System.out.println("L�ufer B gewinnt nach " + (zeit - 1) + "s.");
	}
}
	
