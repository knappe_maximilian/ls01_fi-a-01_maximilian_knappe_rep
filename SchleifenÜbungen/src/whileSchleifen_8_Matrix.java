import java.util.Scanner;
public class whileSchleifen_8_Matrix {

	public static boolean istquersumme(int n, int censZahl)							//Methode zum Feststellen, ob Quersumme des Z�hlers dem Wert der zensierten Zahl entspricht
	{																				//Variable "n" entspricht den Z�hler der main Methode und Variable censZahl enspricht der zensierten Zahl der main Methode
		boolean qs = false;															//Wahrheitswert zum sp�teren Speichern ob Quersumme vorhanden ist
		int summe = 0;																//Variable summe zum Speichern der Quersumme
							
		while (0 != n) {															//Kopfgesteuerte Schleife bis Zahl ist 0
			summe = summe + (n % 10);												//Addiert die letzte Stelle der Zahl auf die Summe
			n = n / 10;																//L�schen der letzten Stelle
		}
		if(summe == censZahl)														//Wenn die Quersumme dem Wert des Z�hlers entspricht
		qs = true; 																	//Wahrheitswert wahr
		
		return qs;																	//�bergabe des Wahrheitswertes zur Weiterverarbeitung zur Verwendung au�erhalb der Methode
	}
	public static boolean istteilbar(int n, int censZahl)							//Methode zum Feststellen, ob Z�hler der main Methode ist durch zensierte Zahl der main Methode teilbar
	{																				//Variable "n" entspricht den Z�hler der main Methode und Variable censZahl enspricht der zensierten Zahl der main Methode
		boolean tb = false;															//Wahrheitswert zum sp�teren Speichern ob ob Z�hler der main Methode ist durch zensierte Zahl der main Methode teilbar
			
		if((n % censZahl == 0) && (n > 1))											//Kontrollstruktur ob Z�hler restlos durch zensierte Zahl teilbar ist
		tb = true;																	//Wenn ja Wahrheitswert true
			
		return tb;																	//�bergabe des Wahrheitswertes zur Weiterverarbeitung zur Verwendung au�erhalb der Methode
	}
	public static boolean istenthalten(int n, int censZahl)							//Methode zum Feststellen, ob zensierte Zahl in Z�hler enthalten ist
	{																				//Variable "n" entspricht den Z�hler der main Methode und Variable censZahl enspricht der zensierten Zahl der main Methode	
		boolean it = false;															//Wahrheitswert zum sp�teren Speichern ob zensierte Zahl in Z�hler enthalten ist

			if(n == censZahl)														//Kontrollstruktur ob Z�hler aus der main Methode der zensierten Zahl aus der main Methode entspricht
			{
				it = true;															//Wenn dem so ist, Wahrheitswert ist wahr
			}
			else if(n % 10 == censZahl)												//Wenn die letzte Ziffer der zensierten Zahl aus der main Methode entspricht
			{
				it = true;															//Wenn dem so ist, Wahrheitswert ist wahr
			}
			
				n = n % 10;															//Abziehen der letzten Ziffer
				
			if(n == censZahl)														//Kontrollstruktur ob die �brige Ziffer der zensierten Zahl aus der main Methode entspricht
				it = true;															//Wenn dem so ist, Wahrheitswert ist wahr

		return it;																	//�bergabe des Wahrheitswertes zur Weiterverarbeitung zur Verwendung au�erhalb der Methode
	}
	
	
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in); 						//Initialisierung Scanner tastatur
		System.out.println("Welche Zahl soll ausgeblendet werden?");	//Ausgabe Frage nach zensierter Zahl
		int zensiertezahl = tastatur.nextInt();							//Eingabe der zensierten Zahl
		int z�hler = 0;													//Z�hler f�r die Kopfgesteuerte Schleife und �bergabewert f�r die drei Methoden "istenthalten", "istteilbar" und "istquersumme"

		boolean istenthalten;											//drei Wahrheitswerte f�r die �bergabe der Wahrheitwerte der drei Methoden
		boolean istteilbar;
		boolean istquersumme;

		System.out.print(" ");
		while(z�hler <= 99)												//Kopfgesteuerte Schleife f�r das Z�hlen der Zahlen 1 bis 99
		{
			istenthalten = false;										//Zur�cksetzen der Wahrheitswerte zum erneuten Durchlaufen der Schleife
			istteilbar = false;
			istquersumme = false;
			
			istenthalten = istenthalten(z�hler,zensiertezahl);			//�bergabe der Wahrheitswerte an die Variablen
			istteilbar = istteilbar(z�hler,zensiertezahl);
			istquersumme = istquersumme(z�hler,zensiertezahl);

			if((z�hler % 10 == 0) && (z�hler > 9))						//n�chste Zeile Kontrollstruktur
				System.out.println(" ");

			if(z�hler < 10)												
			{
				if(istenthalten == true)								//Ausgabe der Zensur, je nach Fall anders formatiert
					System.out.print("*  ");
				else if(istteilbar == true)
					System.out.print("%  ");
				else if(istquersumme == true)
					System.out.print("#  ");
				else
					System.out.print(z�hler + "  ");					//Wenn keines Zensierungskriterien zutriff, dann wird die normale Ziffer entsprechend der Z�hlerwertes an Stelle ausgegeben
			}
			else if(z�hler >= 10)										//dasselbe wie oben, nur anders formatiert
			{
				if(istenthalten == true)
					System.out.print(" * ");
				else if(istteilbar == true)
					System.out.print(" % ");
				else if(istquersumme == true)
					System.out.print(" # ");
				else
					System.out.print(z�hler + " ");
			}
			z�hler++;													//Erh�hung des Z�hlerwertes um eins f�r den n�chsten Schleifendurchlauf
		}
	}
}
