package raumschiffe;

public class Ladung {
	
	//Atttribute/ Variablendeklaration
	private String designation; //Bezeichnung der Ladung als String
	private int amount;			//Menge der Ladung als Integer
	
	//vollparametrisierter Konstruktor Ladung mit �bergabeparameter der Bezeichnung und der Menge
	public Ladung(String designation, int amount) {	
		this.designation = designation;				//Zuweisung Bezeichnung
		this.amount = amount;						//Zuweisung Menge
	}
	
	//Set und Get f�r Schiffsname
	public void setDesignation (String designation2) {
		designation = designation2;
	}
	
	public String getDesignation () {
		return designation;
	}
	
	//Set und Get f�r Menge
	public void setAmount (int amount2) {
		this.amount = amount2;
	}
	
	public int getAmount () {
		return amount;
	}

}
