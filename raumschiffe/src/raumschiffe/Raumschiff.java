package raumschiffe;

import java.util.ArrayList;

public class Raumschiff {

	
	private static final int photonentorpAmount = 0;
	//Variablendeklaration
	private String shipName;
	private int photontorpAmount;
	private int powerPercent;
	private int shieldsPercent;
	private int shellPercent;
	private int lifesystemPercent;
	private int androidAmount;
	private static ArrayList <String> broadcastcommunicator = new ArrayList<String>();
 	private ArrayList<Ladung> loadregister = new ArrayList<Ladung>();
	
 	//Konstruktoren
 	
 	//parameterlos
 	public Raumschiff() {
 	}
 	
	//Konstruktor Raumschiff mit �bergabeparametern (vollparametisiert)
	Raumschiff(int photontorpAmount, int powerPercent,int shieldsPercent,
			int shellPercent, int lifesystemPercent, String shipName, int androidAmount) {
		
		//�bergabe Zuweisung (Schiffsname, Anzahl Photonentoperdos, Energieversorgung in Prozent, Schild in Prozent, 
		//H�lle in Prozent, �berlebenssystem in Prozent und Menge an Androiden)
		this.shipName = shipName;
		this.photontorpAmount= photontorpAmount;
		this.powerPercent = powerPercent;
		this.shieldsPercent = shieldsPercent;
		this.shellPercent = shellPercent;
		this.lifesystemPercent = lifesystemPercent;
		this.androidAmount = androidAmount;
	}
	
	//Erzeugung GETTER und SETTER
	
	public void setshipName (String shipName2) {
		shipName = shipName2;
	}
	
	public String getshipName () {
		return shipName;
	}
	public void setphotontorpAmount (int photontorpAmount) {
		this.photontorpAmount = photontorpAmount;
	}
	
	public int getphotontorpAmount () {
		return photontorpAmount;
	}
	public void setpowerPercent (int powerPercent) {
		this.powerPercent = powerPercent;
	}
	
	public int getpowerPercent () {
		return powerPercent;
	}
	public void setshieldsPercent (int shieldsPercent) {
		this.shieldsPercent = shieldsPercent;
	}
	
	public int getshieldsPercent () {
		return shieldsPercent;
	}
	public void setshellPercent (int shellPercent) {
		this.shellPercent = shellPercent;
	}
	
	public int getshellPercent () {
		return shellPercent;
	}
	public void setlifesystemPercent (int lifesystemPercent) {
		this.lifesystemPercent = lifesystemPercent;
	}
	
	public int getlifesystemPercent () {
		return lifesystemPercent;
	}
	public void setandroidAmount (int androidAmount) {
		this.androidAmount = androidAmount;
	}
	
	public int getandroidAmount() {
		return androidAmount;
	}
	
	//Methode um Ladung einzuladen
	public void addLadung(Ladung neueLadung) {
		this.loadregister.add(neueLadung);
	}
	
	//Zustand Schiff ausgeben
	public void statusShip () {
		System.out.println("Zustand des Raumschiffes " + this.getshipName() + " :" +
						"\nAnzahl der Photontorpedos: " + this.getphotontorpAmount() +
						"\nZustand der Lebenserhaltung " + this.getlifesystemPercent() +
						"\nZustand der Energie " + this.getpowerPercent() +
						"\nZustand der Schilde " + this.getshieldsPercent() +
						"\nZustand der H�lle " + this.getshellPercent() +
						"\nAnzahl der Androiden" + this.getandroidAmount());
	}
	
	//Ladungsregister ausgeben
	public void loadregister() {
		System.out.println("Das Raumschiff " + this.getshipName() + " enthaelt folgende Ladung: ");
		for(Ladung tmp:loadregister) {
			System.out.println(tmp.getDesignation() + " " + tmp.getAmount());
		}
	}
	
	//Treffer ausgeben
	private void hit (Raumschiff h) {
		System.out.println(this.getshipName() + "HIT, Treffer!");
	}
	
	//Treffer berechnen
	private void treffer(Raumschiff h) {
		h.setshieldsPercent(h.getshieldsPercent() -50);
		if (h.getshieldsPercent() <=0) {
			h.setshellPercent(h.getshellPercent() -50);
			h.setpowerPercent(h.getpowerPercent() -50);
			if (h.getshellPercent() <= 0) {
				h.setlifesystemPercent(0);
				messageAll("Lebenserhaltungssystem zerst�rt!");
			}
		}
	}
	
	//[AUFGABE] Phaserkanonen Abschuss, wenn �ber 50% Energie. Sonst Nachricht
	public void phasecanonsShoot(Raumschiff h) {
		if (getpowerPercent() < 50 ) {
			System.out.println("=*Click*=");
		}else {
			setpowerPercent(getpowerPercent() -50);
			System.out.println("Phaserkanone abgeschossen!");
			hit(h);
			treffer(h);
		}
	}
	
	//Photonentorpedo Abschie�en
	public void photontorpShoot (Raumschiff r) {
		if (photonentorpAmount <1) {
			messageAll("Photonentorpedo abgeschossen!");
			treffer(r);
		}
		else if (photonentorpAmount >1)
			System.out.println("Keine Photontorpedos gefunden!");
	}
	
	//Nachricht an Alle
	public void messageAll(String message) {
		broadcastcommunicator.add("\n" + this.shipName + ": " + message);
	}
	
	//Eintr�ge des Logbuches ausgeben
	public static ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastcommunicator;
	}
	

}
