package raumschiffe;

public class Test {

	public static void main(String[] args) {
//Raumschiffvariablen bestimmen
		Raumschiff k = new Raumschiff(1, 100 , 50, 100, 100, "IRW Hegh'ta", 2);
		Raumschiff r = new Raumschiff(2, 50 , 50, 100, 100, "IRW Khazara", 2);
		Raumschiff v = new Raumschiff(0, 80 , 80, 50, 100, "Ni'Var", 5);
		
//Ladung
		Ladung loadk1 = new Ladung("Ferrengi Schneckensaft", 200);
		Ladung loadk2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		
		k.addLadung(loadk1);
		k.addLadung(loadk2);
		
		Ladung loadr1 = new Ladung("Borg-Schrott", 5);
		Ladung loadr2 = new Ladung("Rote Materia", 2);
		Ladung loadr3 = new Ladung("Plasma-Waffe", 50);
		
		r.addLadung(loadr1);
		r.addLadung(loadr2);
		r.addLadung(loadr3);
		
		Ladung loadv1 = new Ladung("Forschungssonde", 35);
		Ladung loadv2 = new Ladung("Photonentorpedo", 2);
		
		v.addLadung(loadv1);
		v.addLadung(loadv2);
		
		//Aktionen durch Methoden aufgerufen
		k.photontorpShoot(r);
		r.phasecanonsShoot(k);
		v.messageAll("Gewalt ist nicht logisch!");
		k.statusShip();
		k.loadregister();
		k.photontorpShoot(r);
		k.photontorpShoot(r);
		k.statusShip();
		r.statusShip();
		v.statusShip();
		k.loadregister();
		r.loadregister();
		v.loadregister();
		
		System.out.println(Raumschiff.eintraegeLogbuchZurueckgeben());
		
	}

}
